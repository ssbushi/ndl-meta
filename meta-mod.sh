#!/bin/bash

#set -e

# firstpage to start at. If 0 then start in the middle of the document unless
# the document only has 1 page
firstpage=2

# maximum nr of pages to parse; 0 means all. May take a while for big books.
maxpages=0

# search string length in number of words. Son't set too big as the script 
# attempts to find this many consecutive *sane* words ... 25 or so should do
searchwords=25

# number of chunks to extract
maxsearches=3

# set in command line
[[ $# -lt 1 ]] && echo "
Syntax: `basename $0` <pdf_file> [firstpage=$firstpage] [maxpages=$maxpages] [searchwords=$searchwords] [maxsearches=$maxsearches] [pdftotext=<path>] [aspell=<path>] [gs=<path>]

firstpage=n     Page number in pdf to start from. If this is 0 then start in the
                middle of the document (default $firstpage)
maxpages=n      Max number of pages to parse looking for words. If this is 0
                then parse all pages starting with firstpage (default $maxpages)
searchwords=n   Max number of consecutive sane words to extract (default $searchwords)
maxsearches=n   Max number of different searches to send to Google Scholar (default $searchwords)
pdftotext=path  Path to pdftotext (if automatic detection fails).
aspell=path     Path to pdftotext (if automatic detection fails).
gs=path         Path to ghostscript (if automatic detection fails). This is only
                required if you use firstpage=0.
" && exit 0

[[ $# -gt 1 ]] && for ((i=2;i<=$#;i++)); do eval 'v=$'$i; eval "${v%%=*}=${v#*=}"; done

FindDep ()
{
	eval 'dep=$'$1
	[[ -z $dep ]] && eval $1'=`which '$1' 2>/dev/null || bogus=1`'
	eval 'dep=$'$1
	if [[ ! -x $dep ]]; then echo "$1 was not found. Install it or specify its path using $1=<path> parameter."; exit 1; fi
}

FindDep pdftotext
FindDep aspell
[[ $firstpage = 0 ]] && FindDep gs

#------------------------------------------------------------------------------

Debug () {
	echo -e "$@" >&2
}

# temp file to hold the extracted text
txt=`mktemp`

# extract text from pdf according to options
if [[ $firstpage = 0 ]]
then
	nrpages=`$gs -q -dNODISPLAY -c "($1) (r) file runpdfbegin pdfpagecount = quit"`
	[[ $nrpages -ge 4 ]] && firstpage=$((nrpages/2))
fi

# max pages specified?
[[ $maxpages -gt 0 ]] && l="-l $((firstpage+maxpages-1))"

# skip to the middle of the pdf to skip any preprending boilerplate pages
$pdftotext -raw -nopgbrk -f $firstpage $l "$1" $txt
	
# cat $txt

# extract the median line length; remove all lines shorter than 30 chars first
# since those can be very numerous because of figures, and distort the median
lines=( `while read l; do [[ ${#l} -gt 30 ]] && echo ${#l}; done < $txt | sort -n` )
num=${#lines[*]}
med=${lines[$((num/2))]}

#Debug ${lines[*]}
#Debug med=$med

# extract chunks of consecutive lines that have lengths within 10% of median
# and then fetch the biggest such chunk
num=0 
hit=0
starts=( )
ends=( )
while read l
do
	let num=num+1
	if [[ ${#l} -ge $(((med*900)/1000)) && ${#l} -le $(((med*1100)/1000)) ]]
	then
		[[ $hit = 0 ]] && hit=$num
	else
		if [[ $hit -gt 0 ]]
		then
			starts=( ${starts[*]} $hit )
			ends=( ${ends[*]} $((num-1)) )
		fi
		hit=0
	fi
done < $txt

# find top 3 biggest chunks by comparing boundaries. sort etc)
maxes=':'
chunks=( )
for ((j=0;j<$maxsearches;j++))
do
	# search for a possible initial max (not in mlist)
	for ((i=0;i<${#starts[*]};i++)); do [[ ! $maxes =~ :$i: ]] && break; done
	max=$i
	# extract next max
	for ((i=1;i<${#starts[*]};i++))
	do
		[[ $((${ends[i]}-${starts[i]})) -gt $((${ends[max]}-${starts[max]})) && ! $maxes =~ :$i: ]] && max=$i
	done
	maxes="$maxes$max:"
	chunks[j]=`sed -n ${starts[max]},${ends[max]}p $txt`
done

#echo "*********************************"

#for i in "${chunks[@]}"
#do
#	echo $i
#done


Sanitize ()
{
	local words len asp i
	words=( $* )
	
	# Delete first and last words as they may be hyphenation remnants
	#unset words[0]
	#unset words[${#words[*]}]
	
	# Do something about hyphens at the end of line; they now appear as "foo- bar"
	# We could concatenate if left side is not all caps or if right side has no
	# caps. This is still prone to errors for words such as "non-trivial" or
	# "state-of-the-art". Latex usually tries to avoid hyphenating already hyphened
	# expressions.
	# Use aspell to check the expression "foobar". The idea is that if latex 
	# hyphenated a word then it did so based on a dictionary which aspell *should*
	# recognize.
	# Force global "en" dictionary, otherwise aspell uses the current locale, which
	# may not match the document (e.g. "visualize" fails on en_GB, my default).
	
	# 2012-07-05: Actually, Google Scholar full-text indexing contains wrong
	# hyphenated words. If you fix the hyphenation, some (most?) quoted
	# searches will fail

	# len=${#words[*]}
	# # Debug ================================
	# # Debug ${words[*]}
	# # Debug ================================
	# for ((i=0;i<len;i++))
	# do
		# if [[ "${words[i]:1,-1}" = "-" ]]
		# then
			# asp=`echo "${words[i]%?}${words[i+1]}" | $aspell list -l en`
			# if [[ -z $asp ]]
			# then
				# words[i]=${words[i]%?}${words[i+1]}
				# unset words[i+1]
				# let i=i+1
			# else
				# words[i]=${words[i]}${words[i+1]}
				# unset words[i+1]
				# let i=i+1
			# fi
		# fi
	# done
	# words=( ${words[*]} )
	# # Debug ================================
	# # Debug ${words[*]}
	# # Debug ================================

	# delete non-sane words (i.e. which have funny chars or numbers; yes, 
	# numbers too since inline maths, funny single quotes, figures etc make a mess)
	len=${#words[*]}
	for ((i=0;i<len;i++))
	do
		[[ "${words[i]}" =~ [^a-zA-Z\,\:\;\(\)\.\-] ]] && unset words[i]
	done
	
	# extract the longest train of consecutive sane words
	local maxlen=0
	local maxidx=0
    local len=$#
	for ((i=0;i<len-1;i++))
	do
		for ((j=i+1;j<len;j++))
		do
			[[ -z ${words[j]} ]] && break
		done
		[[ $((j-i-1)) -gt $maxlen ]] && maxlen=$((j-i-1)) && maxidx=$i
		i=$((j+1))
	done
	[[ $maxlen -gt $searchwords ]] && maxlen=$searchwords
	echo ${words[*]:maxidx:maxlen}
}
	
cook=`mktemp`
html=`mktemp`

# change user agent (yes, don't mention it)
ua="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5"

searches=( )
#urls=( )
#bibtexs=( )
# DO NOT CHANGE THIS FILE NAME. IT WILL CRASH THE TEXT PARSER.
strings='bibResponse.txt'
for ((k=0;k<${#chunks[*]};k++))
do
	searches[k]=`Sanitize ${chunks[k]}`
	#echo "**********************"
	#echo ${searches[k]} >> $strings
	urls[k]=${searches[k]// /+}
	urls[k]=${urls[k]//\;/%3B}
	urls[k]=${urls[k]//,/%2C}
	urls[k]=${urls[k]//\(/%28}
	urls[k]=${urls[k]//\)/%29}
	
	urls[k]="http://scholar.google.com/scholar?q=%22${urls[k]}%22&hl=en"

	#Debug ==================================================================
	#Debug STR: ${searches[k]}
	#Debug URL: ${urls[k]}
	#Debug ==================================================================
	# fetch cookie first so that we can afterwards fetch bibtex entries
	# inspired from http://matela.com.br/pub/scripts/bibscholar-0.3
	wget -q --user-agent="$ua" --spider --save-cookies=$cook "http://scholar.google.com"
	# change cookie, enable bibtexs
	sed -i 's/^\.scholar\.google\..*/&:CF=4/' $cook
	# fetch the id of the max first 3 results
	ids=( `wget -q --user-agent="$ua" --load-cookies=$cook "$urls[k]" -O - | 
		egrep -o "q=info:[^:]+" | uniq | head -n3` )
	# get the bibtex's
	for id in ${ids[*]}
	do
		sleep 1
		bib=`wget -q --user-agent="$ua" --load-cookies=$cook \
			"http://scholar.google.com/scholar.bib?$id:scholar.google.com/&output=citation" -O -`
		bibtexs=( "${bibtexs[@]}" "$bib" )
        echo "$bib">> $strings
	echo "==========================================================" >> $strings
	done
	
	#$firefox $url &
done

# look for a DOI
doi=`egrep -o "10\.[0-9]{4,}\/[^[:space:]]*" $txt`
if [[ ! -z $doi ]]
then
	Debug ==================================================================
	echo DOI found: http://dx.doi.org/$doi
fi

# look for a PII (Publisher Item Identifier - used by a few important publishers,
# among which IEEE, see http://en.wikipedia.org/wiki/Publisher_Item_Identifier
# Edit: there appears to be no search engine or redirector for PIIs. The only
#       working way I found was to search in Google with the quoted PII, but
#       google results are nasty to parse and I don't have the time now

rm -f $cook $html $txt
