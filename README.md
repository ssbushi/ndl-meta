# README #

## Implementation Notes

### Extracting ISBN from the text file
- Relevant functions: findISBN(String ), getISBN(String , String )
- The argument for findISBN(String fileName) is the full text-file path with filename+extension.
- Usage is by calling the findISBN(String ) function.
- findISBN(String fileName) handles the main function calls and searches the "ISBN" string in the document to find the possible ISBN (be it ISBN-10 or ISBN-13).
- The ISBN is searched first in the document and then in the fileName itself. If not found, further processing is done separately.
- Exception handling is done using suitable try-catch blocks.

### Fetching JSON data from Google Books
- Relevant functions: findISBN(String ), readAll(Reader ), readJsonFromUrl(String )
- If some suitable ISBN is believed to be found, then Google Books API calls are made to extract the JSON data associated with the particular ISBN number. 
- Some variation in the query is made so as to definitely produce some result which is further stored in a JSONObject.
- If JSONObject is found to be empty by checking the "totalItems" field, then again further processing is done separately.
- Exception handling is done using suitable try-catch blocks.

### Converting file to text (parsing)
- tika has a limit of 10000 characters. That many characters will be extracted and stored in the text file;
- Name of the text file is "Path/To/File/filename-text.txt". ie, "/home/daivik/Documents/file.pdf" will be parsed to "/home/daivik/Documents/file-text.txt;
- Will work for all supported formats of Tika;
- To use:
	ConvertToText.convert("/home/daivik/Documents/file.pdf",metaData);
	//Where metaData is an instance of EducationalMetaData
- Also, use ConvertToText.setMax(int) to set the maximum number of characters to parse [default is 10000].
- The function will return an instace of EducationalMetaData with the extraced metadata filled in.
- Also using djvutxt from dlibre-bin to convert djvu to text. tika doesnt work, nor retrieves any metadata from properties.


### Extracting metadata from JSON
- call ConvertToText.extractData(JSONObject json, String filePath)
- Function creates a file "filename-meta.txt" in the same directory.

### Extracting book data from HTML files
- Some websites like "http://www.sacred-texts.com" have whole books completely in HTML format. An attempt has been made to parse them.
- As of now, the title, author, keywords, description and year of publication are tried to be extracted.
- Any Dublic-Core metadata fields if present in the html file is also extracted.
- Language or any other field are quite difficult to extract at the moment (commercial APIs exist).
- NER is now used for verifying possible author names.
- Usage is by calling the htmlparse(String ) function of the class with an appropriate URL or filePath.

### MetaDataItem class
- Contains all the meta-data items that are required to be extracted from various documents.
- Most meta-data items are strings with default value null.
- In case any meta-data is not available, the corresponding field is to be left blank(i.e. unchanged)
- A function printing all the meta-data of a MetaDataItem object is also implemented.
- Presently, the legal meta-data is part of the generic meta-data only, can be changed in future if necessary.
- The metaDataType(educational/thesis) of a file MUST be specified while updating the fields, so as to enable the proper functioning of the print function. If not specified, only the generic meta-data will be printed.

### Storing the extracted clean metadata in XML file for future transportation
- Use XMLCreate.XMLMaker() to do the same. Specify the metadata fields according to the given example. Output is an XML file at the specified location. Modify according to usage.

### Parsing Powerpoint files
- PPTParser.java has two methods, parsePPT and parsePPTX both taking the file path as a parameter.
- As is implied from the name, the two functions are to be used to parse .ppt and .pptx files respectively.
- As of now, parsePPTX cannot access Headers and Footers from the file, and any update in this regard would be helpful.
- For parsePPT, it can detect the title, author and creator as well as probable subject and probable date of creation.
- Both these functions are enabled for console printing only, and must be modified to take MetaDataItem object as parameter and called from KuchBhi.java. This can be done by the person who is modifying KuchBhi.java.

### Parsing Word Document files
- DocDetect.java has two methods, parseDoc and parseDocx
- The two functions are to be used to parse .doc and .docx files respectively.
- They take a file path and a MetaDataItem object as parameters 
- Extracts author, organization,Location and Title when possible and fills the object.
- Since it is possible that many people and places will be mentioned in the same document,the following protocol will be applied to decide on which to choose.
> The information in the footer is considered most reliable, 
> Then the header,
> Then the first occurence in the main text.
- The title is the text that appears in the largest font size among the first 20 paragraphs.
- If the corresponding fields in the MetaDataItem are empty, they are updated.

### Special credits to:
- Apache community for TIKA, POI and File-Utilities
- Google Books API
- normadize@zotero-forums for pdf-meta.sh which has been modified by use to suit our needs.
- dLibre-bin tools for djvutxt used in text conversion of djvu files
- Crossref API for DOI reference
- Stanford NER API for doc and ppt parsers.
- tutorialspoint.com
- StackOverflow community