package metaExtract;

import java.io.*;
import java.net.*;
import java.util.*;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import static java.lang.Integer.max;
import static java.lang.Integer.min;
import static metaExtract.MainPage.textFile;

/**
 * Get the source code of a HTML page
 * and parse it to get the title, author, keywords, description and YoP and any other Dublic-Core if present in the meta tags
 * Preference order for title: <title> tag > <h1> > <h2> > <h3> > <h4>
 * Preference order for author: <meta> > <h2> > <h3> > <h4>
 * Preference order for YoP: Brute force > <h3> > <h4>
 * Due to abnormal structuring of webpages and lack of traditional layout, probably only the title can be extracted with decent confidence
 * @author Biswarup Bhattacharya
 * @version 11-Jun-2015
 */

public class HTMLParser
{
    public static List<Triple<String,Integer,Integer>> detect(String text) throws Exception{
        String serializedClassifier = "dependencies/english.muc.7class.distsim.crf.ser.gz";
        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);
        List<Triple<String,Integer,Integer>> triples = classifier.classifyToCharacterOffsets(text);
        return triples;
    }
    
    public static void htmlparsefile(String fileName, MetaDataItem metaData)throws IOException, Exception
    {
        String source = (getSource(fileName));
        String title = "";
        String author = "";
        String keywords = "";
        String dcval = "";
        boolean flagAuthor = false;
        boolean flagTitle = false;
        int yy=0;
        List<Triple<String,Integer,Integer>> footer,header,body;
        if((source.toLowerCase()).contains("<meta name=\"dc.title\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.title\" content=\"");
            i=i+31;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Title: " + dcval);
            metaData.setTitle(title);
            flagTitle = true;
        }
        else if((source.toLowerCase()).contains("<title>"))
        {
            int i = (source.toLowerCase()).lastIndexOf("<title>");
            int j = (source.toLowerCase()).indexOf("</title>");
            title = source.substring(i+7, j);
            System.out.println("Possible Title: " + title);
            metaData.setTitle(title);
            flagTitle = true;
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.creator\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.creator\" content=\"");
            i=i+33;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Creator: " + dcval);
            metaData.setCreator(dcval);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.subject\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.subject\" content=\"");
            i=i+33;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Subject: " + dcval);
            metaData.setSubject(dcval);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.date\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.date\" content=\"");
            i=i+30;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Date of Publication: " + dcval);
            yy = 1;
            metaData.setDateCreated(dcval);
        }
        else if((source.toLowerCase()).contains("<meta name=\"date\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"date\" content=\"");
            i=i+27;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Possible Date of Publication: " + dcval);
            yy = 1;
            metaData.setDateCreated(dcval);
        }
        dcval = "";
        keywords = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.description\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.description\" content=\"");
            i=i+37;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Description: " + dcval);
            metaData.setDescriptionAbstract(dcval);
        }
        else if((source.toLowerCase()).contains("<meta name=\"description\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"description\" content=\"");
            i=i+34;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                keywords = keywords + source.charAt(i);
            }
            System.out.println("Possible Description: " + keywords);
            metaData.setDescriptionAbstract(keywords);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.publisher\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.publisher\" content=\"");
            i=i+35;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Publisher: " + dcval);
            metaData.setPublisher(dcval);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.contributor\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.contributor\" content=\"");
            i=i+37;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Contributor: " + dcval);
            metaData.setEditor(dcval);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.language\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.language\" content=\"");
            i=i+34;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Language: " + dcval);
            metaData.setLanguageISO(dcval);
        }
        dcval = "";
        if((source.toLowerCase()).contains("<meta name=\"dc.relation\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"dc.relation\" content=\"");
            i=i+34;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                dcval = dcval + source.charAt(i);
            }
            System.out.println("Relation: " + dcval);
        }
        if((source.toLowerCase()).contains("<meta name=\"author\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"author\" content=\"");
            i=i+29;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                author = author + source.charAt(i);
            }
            System.out.println("Possible Author: " + author);
            metaData.setAuthor(author);
            flagAuthor = true;
        }
        if((source.toLowerCase()).contains("<meta name=\"keywords\""))
        {
            int i = (source.toLowerCase()).lastIndexOf("<meta name=\"keywords\" content=\"");
            i=i+31;
            for(;;i++)
            {
                if((source.charAt(i))== '\"')
                    break;
                keywords = keywords + source.charAt(i);
            }
            System.out.println("Possible Keywords: " + keywords);
        }

        int h1 = 0, h2 = 0, h3 = 0, h4 = 0;
        if((source.toLowerCase()).contains("<h1"))
        {
            int i = (source.toLowerCase()).lastIndexOf("<h1");
            i=i+3;
            int start=0;
            title = "";
            for(;;i++)
            {
                if((source.charAt(i))== '<')
                    break;
                if(start==1)
                    title = title + source.charAt(i);
                if((source.charAt(i))== '>' && start==0)
                    start=1;
            }
            h1 = 1;
            if((!flagTitle))
            {
                System.out.println("Possible Title: " + title);
                metaData.setTitle(title);
                flagTitle = true;
            }
        }
        if((source.toLowerCase()).contains("<h2"))
        {
            int i = (source.toLowerCase()).lastIndexOf("<h2");
            i=i+3;
            int start=0;
            title = "";
            for(;;i++)
            {
                if((source.charAt(i))== '<')
                    break;
                if(start==1)
                    title = title + source.charAt(i);
                if((source.charAt(i))== '>' && start==0)
                    start=1;
            }
            h2 = 1;
            if(h1==0)
            {
                if((!flagTitle))
                {
                    System.out.println("Possible Title: " + title);
                    metaData.setTitle(title);
                    flagTitle = true;
                }
            }
            else
            {
                try
                {
                    footer = detect(title);
                    for(Triple<String,Integer,Integer> trip:footer)
                    {
                        if(trip.first.equalsIgnoreCase("Person") && !(flagAuthor))
                        {
                            title=title.substring(trip.second, trip.third);
                            flagAuthor = true;
                            System.out.println("Possible Author: " + title);
                            metaData.setAuthor(title);
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        int k=0;
        String year = "";
        for(;k<source.length();k++)
        {
            if(Character.isDigit(source.charAt(k)) && Character.isDigit(source.charAt(k+1))
                && Character.isDigit(source.charAt(k+2)) && Character.isDigit(source.charAt(k+3)))
            {
                year = year + source.charAt(k) + source.charAt(k+1) + source.charAt(k+2) + source.charAt(k+3);
                System.out.println("Possible Year of Publication: " + year);
                metaData.setDateCreated(year);
                yy=1;
                break;
            }
        }
        if((source.toLowerCase()).contains("<h3"))
        {
            int i = (source.toLowerCase()).lastIndexOf("<h3");
            i=i+3;
            int start=0;
            title = "";
            for(;;i++)
            {
                if((source.charAt(i))== '<')
                    break;
                if(start==1)
                    title = title + source.charAt(i);
                if((source.charAt(i))== '>' && start==0)
                    start=1;
            }
            h3 = 1;
            if(h1==0 && h2==0)
            {
                if((!flagTitle))
                {
                    System.out.println("Possible Title: " + title);
                    metaData.setTitle(title);
                    flagTitle = true;
                }
            }
            else if(h1==1 && h2==0)
            {
                try
                {
                    footer = detect(title);
                    for(Triple<String,Integer,Integer> trip:footer)
                    {
                        if(trip.first.equalsIgnoreCase("Person") && !(flagAuthor))
                        {
                            title=title.substring(trip.second, trip.third);
                            flagAuthor = true;
                            System.out.println("Possible Author: " + title);
                            metaData.setAuthor(title);
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if(h1==0 && h2==1)
            {
                try
                {
                    footer = detect(title);
                    for(Triple<String,Integer,Integer> trip:footer)
                    {
                        if(trip.first.equalsIgnoreCase("Person") && !(flagAuthor))
                        {
                            title=title.substring(trip.second, trip.third);
                            flagAuthor = true;
                            System.out.println("Possible Author: " + title);
                            metaData.setAuthor(title);
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if(yy==0)
            {
                System.out.println("Possible Year of Publication: " + title);
                metaData.setDateCreated(title);
            }
        }
        if((source.toLowerCase()).contains("<h4"))
        {
            int i = (source.toLowerCase()).lastIndexOf("<h4");
            i=i+3;
            int start=0;
            title = "";
            for(;;i++)
            {
                if((source.charAt(i))== '<')
                    break;
                if(start==1)
                    title = title + source.charAt(i);
                if((source.charAt(i))== '>' && start==0)
                    start=1;
            }
            h4 = 1;
            if(h1==0 && h2==0 && h3==0)
            {
                if((!flagTitle))
                {
                    System.out.println("Possible Title: " + title);
                    metaData.setTitle(title);
                    flagTitle = true;
                }
            }
            else if((h1==1 && h2==0 && h3==0) || (h1==0 && h2==1 && h3==0) || (h1==0 && h2==1 && h3==0))
            {
                try
                {
                    footer = detect(title);
                    for(Triple<String,Integer,Integer> trip:footer)
                    {
                        if(trip.first.equalsIgnoreCase("Person") && !(flagAuthor))
                        {
                            title=title.substring(trip.second, trip.third);
                            flagAuthor = true;
                            System.out.println("Possible Author: " + title);
                            metaData.setAuthor(title);
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            else if((h1==1 && h2==1 && h3==0 && yy==0) || (h1==0 && h2==1 && h3==1 && yy==0) || (h1==1 && h2==0 && h3==1 && yy==0))
            {
                System.out.println("Possible Year of Publication: " + title);
                metaData.setDateCreated(title);
            }
        }
        if(flagAuthor == false)
        {
            try
            {
                footer = detect(title);
                for(Triple<String,Integer,Integer> trip:footer)
                {
                    if(trip.first.equalsIgnoreCase("Person") && !(flagAuthor))
                    {
                        title=title.substring(trip.second, trip.third);
                        flagAuthor = true;
                        System.out.println("Possible Author: " + title);
                        metaData.setAuthor(title);
                    }
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        int index = 0;
        if(flagAuthor == false)
        {
            try
            {
                if(textFile.toLowerCase().contains("by"))
                {
                    index = textFile.indexOf("by");
                    index += 3;
                    for(int i = index; textFile.charAt(i)!='\n'; i++)
                    {
                        author = author + textFile.charAt(i);
                    }
                    metaData.setAuthor(author.trim());
                    flagAuthor = true;
                }
                
            } catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        if(flagAuthor == false)
        {
            try
            {
                if(textFile.substring(min(1000, textFile.length())).toLowerCase().contains("-"))
                {
                    index = textFile.indexOf("-");
                    index += 2;
                    for(int i = index; textFile.charAt(i)!='\n'; i++)
                    {
                        author = author + textFile.charAt(i);
                    }
                    metaData.setAuthor(author.trim());
                    flagAuthor = true;
                }
                
            } catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        String toc = "";
        try
        {
            if(textFile.substring(index, min(1000, textFile.length()-1)).toLowerCase().contains("content"))
            {
                index = textFile.lastIndexOf("content");
                index += 8;
                for(int i = index; textFile.charAt(i)!='\n'; i++)
                {
                    toc += textFile.charAt(i);
                }
                metaData.setTableOfContents(toc.trim());
            }

        } catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String getUrlSource(String url) throws IOException
    {
        URL yahoo = new URL(url);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.3.100.207", 8080));
        URLConnection yc = yahoo.openConnection(proxy);
        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            a.append(inputLine);
        in.close();
        return a.toString();
    }
    
    public static String getSource(String fileName) throws IOException
    {
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            a.append(inputLine);
        in.close();
        return a.toString();
    }
}
