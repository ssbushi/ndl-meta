package metaExtract;

import static metaExtract.MainPage.filePath;
import static metaExtract.MainPage.metaDataObject;
import org.codehaus.plexus.util.FileUtils;

public class updateMetadataObject {

    public updateMetadataObject(MetaDataItem tempMetadata) {

        if ((metaDataObject.getAuthor() == null || metaDataObject.getAuthor().length() == 0) && (tempMetadata.getAuthor() != null && tempMetadata.getAuthor().length() != 0)) {
            metaDataObject.setAuthor(tempMetadata.getAuthor());
        }

        if ((metaDataObject.getPublisher() == null || metaDataObject.getPublisher().length() == 0) && (tempMetadata.getPublisher() != null && tempMetadata.getPublisher().length() != 0)) {
            metaDataObject.setPublisher(tempMetadata.getPublisher());
        }

        if ((metaDataObject.getCreator() == null || metaDataObject.getCreator().length() == 0) && (tempMetadata.getCreator() != null && tempMetadata.getCreator().length() != 0)) {
            metaDataObject.setCreator(tempMetadata.getCreator());
        }

        if ((metaDataObject.getEditor() == null || metaDataObject.getEditor().length() == 0) && (tempMetadata.getEditor() != null && tempMetadata.getEditor().length() != 0)) {
            metaDataObject.setEditor(tempMetadata.getEditor());
        }

        if ((metaDataObject.getDateCreated() == null || metaDataObject.getDateCreated().length() == 0) && (tempMetadata.getDateCreated() != null && tempMetadata.getDateCreated().length() != 0)) {
            metaDataObject.setDateCreated(tempMetadata.getDateCreated());
        }

        if ((metaDataObject.getDateAccessioned() == null || metaDataObject.getDateAccessioned().length() == 0) && (tempMetadata.getDateAccessioned() != null && tempMetadata.getDateAccessioned().length() != 0)) {
            metaDataObject.setDateAccessioned(tempMetadata.getDateAccessioned());
        }

        if ((metaDataObject.getDateCopyright() == null || metaDataObject.getDateCopyright().length() == 0) && (tempMetadata.getDateCopyright() != null && tempMetadata.getDateCopyright().length() != 0)) {
            metaDataObject.setDateCopyright(tempMetadata.getDateCopyright());
        }

        if ((metaDataObject.getUri() == null || metaDataObject.getUri().length() == 0) && (tempMetadata.getUri() != null && tempMetadata.getUri().length() != 0)) {
            metaDataObject.setUri(tempMetadata.getUri());
        }

        if ((metaDataObject.getISBN() == null || metaDataObject.getISBN().length() == 0) && (tempMetadata.getISBN() != null && tempMetadata.getISBN().length() != 0)) {
            metaDataObject.setISBN(tempMetadata.getISBN());
        }

        if ((metaDataObject.getISSN() == null || metaDataObject.getISSN().length() == 0) && (tempMetadata.getISSN() != null && tempMetadata.getISSN().length() != 0)) {
            metaDataObject.setISSN(tempMetadata.getISSN());
        }

        if ((metaDataObject.getCitation() == null || metaDataObject.getCitation().length() == 0) && (tempMetadata.getCitation() != null && tempMetadata.getCitation().length() != 0)) {
            metaDataObject.setCitation(tempMetadata.getCitation());
        }

        if ((metaDataObject.getDescriptionAbstract() == null || metaDataObject.getDescriptionAbstract().length() == 0) && (tempMetadata.getDescriptionAbstract() != null && tempMetadata.getDescriptionAbstract().length() != 0)) {
            metaDataObject.setDescriptionAbstract(tempMetadata.getDescriptionAbstract());
        }

        if ((metaDataObject.getTableOfContents() == null || metaDataObject.getTableOfContents().length() == 0) && (tempMetadata.getTableOfContents() != null && tempMetadata.getTableOfContents().length() != 0)) {
            metaDataObject.setTableOfContents(tempMetadata.getTableOfContents());
        }

        if ((metaDataObject.getLanguageISO() == null || metaDataObject.getLanguageISO().length() == 0) && (tempMetadata.getLanguageISO() != null && tempMetadata.getLanguageISO().length() != 0)) {
            metaDataObject.setLanguageISO(tempMetadata.getLanguageISO());
        }

        if ((metaDataObject.getSubject() == null || metaDataObject.getSubject().length() == 0) && (tempMetadata.getSubject() != null && tempMetadata.getSubject().length() != 0)) {
            metaDataObject.setSubject(tempMetadata.getSubject());
        }

        if ((metaDataObject.getTitle() == null || metaDataObject.getTitle().length() == 0) && (tempMetadata.getTitle() != null && tempMetadata.getTitle().length() != 0)) {
            boolean found = false;
            String lines[] = MainPage.textFile.split("\\n");
            for (String line : lines) {
                String currentLine = line;
                currentLine = currentLine.replaceAll("\\s+", "");
                String temp = tempMetadata.getTitle().replaceAll("\\s+", "");
                if (currentLine.length() > 5) {

                    if (EnhanceBibTex.similarity(temp.toLowerCase(), currentLine.toLowerCase()) > 0.7) //tries to search for "ISBN"
                    {
                        System.out.println("Verified Title TM");
                        found = true;
                        metaDataObject.setTitle(tempMetadata.getTitle());
                        break;
                    }
                }

            }
            if (FileUtils.getExtension(filePath).equals("pdf")) {
                if (!found) {
                    System.out.println("Not Verified Title TM");
                    String check = PDFTools.OCRPDF(filePath, 1, 3);

                    lines = check.split("\\n");
                    for (String line : lines) {
//                    System.out.println(" i am  in title verifier");
                        String currentLine = line;
                        currentLine = currentLine.replaceAll("\\s+", "");
                        String temp = tempMetadata.getTitle().replaceAll("\\s+", "");
                        if (currentLine.length() > 5) {

                            if (EnhanceBibTex.similarity(temp.toLowerCase(), currentLine.toLowerCase()) > 0.7) //tries to search for "ISBN"
                            {
                                System.out.println("Verified through OCR");
                                metaDataObject.setTitle(tempMetadata.getTitle());
                                break;
                            }
                        }

                    }
                }
            }

        }

        if ((metaDataObject.getType() == null || metaDataObject.getType().length() == 0) && (tempMetadata.getType() != null && tempMetadata.getType().length() != 0)) {
            metaDataObject.setType(tempMetadata.getType());
        }

        if (metaDataObject.getNumberOfPages() == 0) {
            metaDataObject.setNumberOfPages(tempMetadata.getNumberOfPages());
        }

        if ((metaDataObject.getMimeType() == null || metaDataObject.getMimeType().length() == 0) && (tempMetadata.getMimeType() != null && tempMetadata.getMimeType().length() != 0)) {
            metaDataObject.setMimeType(tempMetadata.getMimeType());
        }

        if ((metaDataObject.getSource() == null || metaDataObject.getSource().length() == 0) && (tempMetadata.getSource() != null && tempMetadata.getSource().length() != 0)) {
            metaDataObject.setSource(tempMetadata.getSource());
        }

        if ((metaDataObject.getEducationalLevel() == null || metaDataObject.getEducationalLevel().length() == 0) && (tempMetadata.getEducationalLevel() != null && tempMetadata.getEducationalLevel().length() != 0)) {
            metaDataObject.setEducationalLevel(tempMetadata.getEducationalLevel());
        }

        if ((metaDataObject.getTypeOfLearningMaterial() == null || metaDataObject.getTypeOfLearningMaterial().length() == 0) && (tempMetadata.getTypeOfLearningMaterial() != null && tempMetadata.getTypeOfLearningMaterial().length() != 0)) {
            metaDataObject.setTypeOfLearningMaterial(tempMetadata.getTypeOfLearningMaterial());
        }

        if ((metaDataObject.getTypicalLearningTime() == null || metaDataObject.getTypicalLearningTime().length() == 0) && (tempMetadata.getTypicalLearningTime() != null && tempMetadata.getTypicalLearningTime().length() != 0)) {
            metaDataObject.setTypicalLearningTime(tempMetadata.getTypicalLearningTime());
        }

        if ((metaDataObject.getDifficultyLevel() == null) && (tempMetadata.getDifficultyLevel() != null)) {
            metaDataObject.setDifficultyLevel(tempMetadata.getDifficultyLevel());
        }

        if ((metaDataObject.getBoard() == null || metaDataObject.getBoard().length() == 0) && (tempMetadata.getBoard() != null && tempMetadata.getBoard().length() != 0)) {
            metaDataObject.setBoard(tempMetadata.getBoard());
        }

        if ((metaDataObject.getPrerequisiteTopic() == null || metaDataObject.getPrerequisiteTopic().length() == 0) && (tempMetadata.getPrerequisiteTopic() != null && tempMetadata.getPrerequisiteTopic().length() != 0)) {
            metaDataObject.setPrerequisiteTopic(tempMetadata.getPrerequisiteTopic());
        }

        if ((metaDataObject.getPedagogicObjective() == null || metaDataObject.getPedagogicObjective().length() == 0) && (tempMetadata.getPedagogicObjective() != null && tempMetadata.getPedagogicObjective().length() != 0)) {
            metaDataObject.setPedagogicObjective(tempMetadata.getPedagogicObjective());
        }

        if ((metaDataObject.getResearcher() == null || metaDataObject.getResearcher().length() == 0) && (tempMetadata.getResearcher() != null && tempMetadata.getResearcher().length() != 0)) {
            metaDataObject.setResearcher(tempMetadata.getResearcher());
        }

        if ((metaDataObject.getKeyword() == null || metaDataObject.getKeyword().length() == 0) && (tempMetadata.getKeyword() != null && tempMetadata.getKeyword().length() != 0)) {
            metaDataObject.setKeyword(tempMetadata.getKeyword());
        }

        if ((metaDataObject.getAdvisor() == null || metaDataObject.getAdvisor().length() == 0) && (tempMetadata.getAdvisor() != null && tempMetadata.getAdvisor().length() != 0)) {
            metaDataObject.setAdvisor(tempMetadata.getAdvisor());
        }

        if ((metaDataObject.getPlace() == null || metaDataObject.getPlace().length() == 0) && (tempMetadata.getPlace() != null && tempMetadata.getPlace().length() != 0)) {
            metaDataObject.setPlace(tempMetadata.getPlace());
        }

        if ((metaDataObject.getDOI() == null || metaDataObject.getDOI().length() == 0) && (tempMetadata.getDOI() != null && tempMetadata.getDOI().length() != 0)) {
            metaDataObject.setDOI(tempMetadata.getDOI());
        }

        if ((metaDataObject.getInstitution() == null || metaDataObject.getInstitution().length() == 0) && (tempMetadata.getInstitution() != null && tempMetadata.getInstitution().length() != 0)) {
            metaDataObject.setInstitution(tempMetadata.getInstitution());
        }

        if ((metaDataObject.getAwarded() == null || metaDataObject.getAwarded().length() == 0) && (tempMetadata.getAwarded() != null && tempMetadata.getAwarded().length() != 0)) {
            metaDataObject.setAwarded(tempMetadata.getAwarded());
        }

        if ((metaDataObject.getDegree() == null || metaDataObject.getDegree().length() == 0) && (tempMetadata.getDegree() != null && tempMetadata.getDegree().length() != 0)) {
            metaDataObject.setDegree(tempMetadata.getDegree());
        }

        if ((metaDataObject.getDepartment() == null || metaDataObject.getDepartment().length() == 0) && (tempMetadata.getDepartment() != null && tempMetadata.getDepartment().length() != 0)) {
            metaDataObject.setDepartment(tempMetadata.getDepartment());
        }

    }
}
