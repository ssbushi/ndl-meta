package metaExtract;

import edu.stanford.nlp.util.Triple;
import java.io.*;
import java.util.List;
import java.util.logging.*;

public class Supervisor {

    public static String getSupervisor() {
        boolean flag = false;
        try {
            String line = MainPage.textFile;
            if (line.toLowerCase().contains("advis") || line.toLowerCase().contains("supervis") || line.toLowerCase().contains("guid")) {
                int index;
                if (line.toLowerCase().contains("advis")) {
                    index = line.toLowerCase().indexOf("advis");
                } else if (line.toLowerCase().contains("guid")) {
                    index = line.toLowerCase().indexOf("guid");
                } else {
                    index = line.toLowerCase().indexOf("supervis");
                }
                String text = line.substring(index - 50,index + 50);
                String s = "";
                List<Triple<String, Integer, Integer>> triple = PPTParser.detect(text);
                s = triple.stream().filter((triple1) -> (triple1.first.equalsIgnoreCase("Person") && !flag)).map((triple1) -> ", " + text.substring(triple1.second, triple1.third)).reduce(s, String::concat);
                if(s != null)
                    s = s.substring(2);
                if (!s.trim().equals("")) {
                    return s;
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(Supervisor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
