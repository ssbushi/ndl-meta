package metaExtract;

import edu.stanford.nlp.util.Triple;
import java.io.*;
import static java.lang.Integer.min;
import java.util.*;
import java.util.logging.*;
import static metaExtract.DocDetect.detect;
import org.apache.commons.io.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.JSONObject;
import static org.json.JSONObject.NULL;

public class MainPage {

    public static String cwd;
    //Current Working Project Directory
    public static File corePath;
    //Name of the file with extension
    public static String fileName;
    //Complete path of the file
    public static String filePath;
    //path to the text converted file
    public static String textFile;
    public static String filePathTerminal;
    //filepath for temporary pdf created during metadata extraction
    public static String filePathSplit;
    public JSONObject json1 = null;
    boolean JSONrecievedfromISBN = false;
    boolean JSONrecievedfromDOI = false;
    boolean BibtexrecievedfromAbstract = false;
    public static MetaDataItem metaDataObject;
    //extension of the given file
    public static String fileExtension;

    public MainPage(String fileNAME, String c1path) throws Exception {

        metaDataObject = new MetaDataItem();
        cwd = System.getProperty("catalina.home") + "/webapps/ROOT/WEB-INF";
        fileName = fileNAME;
        corePath = new File(c1path);
        filePath = corePath + "/" + fileName;
        fileExtension = FilenameUtils.getExtension(filePath);

        System.setProperty("http.proxyHost", "10.3.100.207");
        System.setProperty("http.proxyPort", "8080");
        System.setProperty("https.proxyHost", "10.3.100.207");
        System.setProperty("https.proxyPort", "8080");

        try {
            if (fileExtension.equals("pdf") || fileExtension.equals("doc")
                    || fileExtension.equals("ppt") || fileExtension.equals("pptx")
                    || fileExtension.equals("docx") || fileExtension.equals("djvu")
                    || fileExtension.equals("epub") || fileExtension.equals("html")
                    || fileExtension.equals("htm")) {

                System.out.println("Valid file : Processing Started\n" + fileName + ":");
                String replaceAll = fileName.replaceAll("[^a-zA-Z0-9\\._+-]", "\\\\$0");
                filePathTerminal = corePath + "/" + replaceAll;
                filePathSplit = corePath + "/outputsplit.pdf";

                try {
                    MetaDataItem updatedMetaData = new MetaDataItem();
                    ConvertToText.convert(updatedMetaData);
                    new updateMetadataObject(updatedMetaData);
                } catch (Exception e) {
                    System.out.println("\nCouldn't convert to text\n");
//                    throw e;
                }
                
                 System.out.println("length :" + textFile.length());

                if (fileExtension.equals("pdf")) {
                    PDDocument pdDoc = PDFTools.readyPDF(filePath);
                    if (metaDataObject.getNumberOfPages() < 1) {
                        metaDataObject.setNumberOfPages(pdDoc.getNumberOfPages());
                    }

                    System.out.println("length :" + textFile.length());
                    if (textFile == null || textFile.equals("")) {
                        System.out.println("i am in ocr");
                        textFile = PDFTools.OCRPDF(filePath, 1, min(30, metaDataObject.getNumberOfPages()));
                        System.out.println("length :" + textFile.length());
                    }
                    if (textFile.length() > 5000) {
                        String temp5 = textFile.substring(0, 5000);
                        if (temp5.toLowerCase().indexOf("abstract") > 0 || temp5.toLowerCase().indexOf("summary") > 0) {
                            if (temp5.toLowerCase().indexOf("abstract") > 0) {
                                temp5 = temp5.substring(temp5.toLowerCase().indexOf("abstract") + 8);
                            } else {
                                temp5 = temp5.substring(temp5.toLowerCase().indexOf("summary") + 7);
                            }
                            if (temp5.length() > 20) {
                                String newline = System.getProperty("line.separator");
                                int index = temp5.toLowerCase().indexOf(newline + newline + newline);
                                while (index < 5 && index > 0) {
                                    temp5 = temp5.substring(1);
                                    index = temp5.toLowerCase().indexOf(newline + newline + newline);
                                }
                                System.out.println("index :" + index);
                                if (index > 150) {
                                    metaDataObject.setDescriptionAbstract(temp5.substring(0, index).replaceAll(newline, " "));
                                }
                            }

                        }
                    }

//                    MainPage.textFile = MainPage.textFile.replaceAll("\\n", " ");
                    /*
                     *Trying searching ISBN in  the text and 
                     *quering google books for that
                     */
                    try {
                        json1 = findISBNJSON.findISBN();
                        if (json1 != null && json1 != NULL) {
                            JSONrecievedfromISBN = true;
                            MetaDataItem updatedMetaData = new MetaDataItem();
                            ConvertToText.extractData(json1, updatedMetaData);
                            new updateMetadataObject(updatedMetaData);
                        } else {
                            System.out.println("\nUnable to find ISBN!\n");
                        }
                    } catch (Exception e) {
                        System.out.println("\nUnable to find ISBN!\n");
                    }

                    if (!JSONrecievedfromISBN) {
                        try {
                            json1 = DOIParser.fetchDOIData();
                            if (json1 != null || json1 != NULL) {
                                JSONrecievedfromDOI = true;
                                MetaDataItem updatedMetaData = new MetaDataItem();
                                ConvertToText.extractDatafromDOIJSONObject(json1, updatedMetaData);
                                new updateMetadataObject(updatedMetaData);
                            }

                        } catch (Exception doiexcep) {
                            System.out.println("\nUnable to find DOI!\n");
                        }
                    }

                    if (!JSONrecievedfromISBN && !JSONrecievedfromDOI) {
                        ArrayList<BibTexData> Bibtex = null;
                        Bibtex = METAMOD.fetchBibData(textFile);
                        System.out.println("Size of ArrayList<BibTexData> Bibtex : " + Bibtex.size());
                        if (Bibtex != null && Bibtex.size() > 0) {
                            try {
                                cleaningBibTex(Bibtex, textFile);
                                if (!BibtexrecievedfromAbstract) {
                                    String check = PDFTools.OCRPDF(filePath, 1, 3);
                                    cleaningBibTex(Bibtex, check);
                                }
                            } catch (Exception e) {

                            }
                        }

                        if (!BibtexrecievedfromAbstract) {
                            System.out.println("No BibTEX");
                            if (metaDataObject.getNumberOfPages() < 30) {
                                try {
                                    String title = getTitleforPDF.getTitle(filePath);
                                    metaDataObject.setTitle(title);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (metaDataObject.getNumberOfPages() > 0) {
                                try {
                                    PDFTools.splitThis(filePathSplit, 1, 5);
                                    String title = getTitleforPDF.getTitle(filePathSplit);
                                    metaDataObject.setTitle(title);
                                } catch (Exception ex) {
                                    Logger.getLogger(MainPage.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            try {
                                String text = textFile;

                                List<Triple<String, Integer, Integer>> tags = DocDetect.detect(text);
                                boolean author = false;
                                boolean organisation = false;
                                boolean date = false;
                                boolean location = false;
                                for (Triple<String, Integer, Integer> trip : tags) {
                                    if (trip.first().equalsIgnoreCase("person") && !author) {
                                        author = true;
//                                        System.out.println("Author=" + text.substring(trip.second(), trip.third()));
//                                        System.out.println("Researcher=" + text.substring(trip.second(), trip.third()));
                                        String author1 = text.substring(trip.second(), trip.third());
                                        System.out.println(trip.third());
                                        String sub = text.substring(trip.third(), min(trip.third() + 100, text.length() - 1));
                                        List<Triple<String, Integer, Integer>> body1 = detect(sub);
                                        for (Triple<String, Integer, Integer> trip1 : body1) {
                                            if (trip1.first.equalsIgnoreCase("Person")) {
                                                author1 += ", " + sub.substring(trip1.second(), trip1.third());
                                            }

                                        }
                                        author1 = author1.replaceAll("\\d+", "");
//                                        for (int i = 0; i < author1.length(); i++) {
//                                            if( author1.charAt(i) <= '9' && author1.charAt(i) >='0'){
//                                                author1 = author1.replace(author1.charAt(i), '');
//                                            }
//                                        }
                                        metaDataObject.setAuthor(author1);
//                                        metaDataObject.setResearcher(metaDataObject.getAuthor());
                                    } else if (trip.first().equalsIgnoreCase("organization") && !organisation) {
                                        organisation = true;
//                                        System.out.println("Organization=" + text.substring(trip.second(), trip.third()));
                                        metaDataObject.setInstitution(text.substring(trip.second(), trip.third()));
                                    } else if (trip.first().equalsIgnoreCase("date") && !date && metaDataObject.getDateCreated().equals("")) {
                                        date = true;
//                                        System.out.println("Date=" + text.substring(trip.second(), trip.third()));
                                        if (!metaDataObject.getDateCreated().equals("")) {
                                            metaDataObject.setDateCreated(text.substring(trip.second(), trip.third()));
                                        }
                                    } else if (trip.first().equalsIgnoreCase("location") && !location) {
                                        location = true;
//                                        System.out.println("Place=" + text.substring(trip.second(), trip.third()));
                                        metaDataObject.setPlace(text.substring(trip.second(), trip.third()));
                                    }
                                    if (location && date && author && organisation) {
                                        break;
                                    }
                                }

                                String line = text.substring(0, min(4000, text.length() - 1));
                                if (line.toLowerCase().contains("thesis") || line.toLowerCase().contains("dissertation")) {
                                    System.out.println(" i found thesis");
                                    metaDataObject.setResearcher(metaDataObject.getAuthor());
                                    metaDataObject.setType("thesis");
                                    metaDataObject.setAdvisor(Supervisor.getSupervisor());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    //For Table of Contents
                    try {
                        String toc = TOCBuilder.getTOC();
                        if (toc != null) {
                            metaDataObject.setTableOfContents(toc);
                        }
                    } catch (Exception e) {

                    }

                    //For References
                    if (metaDataObject.getNumberOfPages() < 50) {
                        metaDataObject.setRefersTo(References.getReferences(filePath));
                    } else if (metaDataObject.getNumberOfPages() > 0) {
                        try {
                            int temp = metaDataObject.getNumberOfPages() - 50;
//                            ProcessBuilder pbuilder = new ProcessBuilder("bash", "-c", "pdftk " + filePathTerminal + " cat " + temp + "-" + metaDataObject.getNumberOfPages() + " output " + filePathSplit);
//                            Process p = pbuilder.start();
//                            p.waitFor();
                            PDFTools.splitThis(filePathSplit, temp, metaDataObject.getNumberOfPages());
                            metaDataObject.setRefersTo(References.getReferences(filePathSplit));
                        } catch (IOException ex) {
                            Logger.getLogger(MainPage.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
//                    System.out.println("MetaData for " + fileName + " : ");
//                    metaDataObject.printMetadata();
                }
                if (fileExtension.equals("docx")) {
                    try {
                        MetaDataItem updatedMetaData = new MetaDataItem();
                        DocDetect.parseDocx(filePath, updatedMetaData);
                        new updateMetadataObject(updatedMetaData);

                    } catch (Exception ex) {
                        Logger.getLogger(MainPage.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
//                        System.out.println("MetaData for " + fileName + " : ");
//                        metaDataObject.printMetadata();
                    }
                }
                if (fileExtension.equals("doc")) {
                    try {
                        MetaDataItem updatedMetaData = new MetaDataItem();
                        DocDetect.parseDoc(filePath, updatedMetaData);
                        new updateMetadataObject(updatedMetaData);

                    } catch (Exception ex) {
                        Logger.getLogger(MainPage.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
//                        System.out.println("MetaData for " + fileName + " : ");
//                        metaDataObject.printMetadata();
                    }
                }
                if (fileExtension.equals("ppt")) {
                    MetaDataItem updatedMetaData = new MetaDataItem();
                    PPTParser.parsePPT(updatedMetaData);
                    new updateMetadataObject(updatedMetaData);
//                    System.out.println("MetaData for " + fileName + " : ");
//                    metaDataObject.printMetadata();
                }
                if (fileExtension.equals("pptx")) {
                    MetaDataItem updatedMetaData = new MetaDataItem();
                    PPTParser.parsePPTX(updatedMetaData);
                    new updateMetadataObject(updatedMetaData);
//                    System.out.println("MetaData for " + fileName + " : ");
//                    metaDataObject.printMetadata();
                }
                if (fileExtension.equals("html") || fileExtension.equals("htm")) {

                    HTMLParser.htmlparsefile(filePath, metaDataObject);
                    metaDataObject.setMimeType("text/html");
//                    System.out.println("MetaData for " + fileName + " : ");
//                    metaDataObject.printMetadata();
                }
                if (fileExtension.equals("epub")) {

                    EbookParser.getZipFiles(filePath);
                    metaDataObject.setMimeType("application/epub+zip");
//                    System.out.println("MetaData for " + fileName + " : ");
//                    metaDataObject.printMetadata();
                }
                try {
                    String key = Wikifier.findKeyWords();
                    metaDataObject.setKeyword(key);
                } catch (Exception e) {

                }
                try {
                    if (metaDataObject.getDescriptionAbstract() != null && metaDataObject.getDescriptionAbstract().length() > 0) {
//                        System.out.println("here :" + metaDataObject.getDescriptionAbstract());
                        LangDetect.LangGet(metaDataObject.getDescriptionAbstract().substring(0, min(30, metaDataObject.getDescriptionAbstract().length() - 1)), "e8f7bdfb007b8a8ec94ffe37ce249248");
                    } 
                    else if (metaDataObject.getTitle() != null) {
                        LangDetect.LangGet(metaDataObject.getTitle(), "e8f7bdfb007b8a8ec94ffe37ce249248");
                    }
                    else {
                        LangDetect.LangGet(textFile.substring(min(1000, textFile.length())), "e8f7bdfb007b8a8ec94ffe37ce249248");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                try 
                {
                    boolean x;
                    try
                    {
                        if (metaDataObject.getDescriptionAbstract() != null || metaDataObject.getDescriptionAbstract().length() > 0) {
                            x = SubjectExtract.SubjectGet(metaDataObject.getDescriptionAbstract().substring(0, min(500, metaDataObject.getDescriptionAbstract().length() - 1)));
                        }
                    }
                    catch(Exception ex)
                    {
                        
                        try
                        {
                            if (metaDataObject.getTitle() != null) {
                                x = SubjectExtract.SubjectGet(metaDataObject.getTitle());
                            }
                        }
                        catch(Exception ex1)
                        {
                            try
                            {
                                x = SubjectExtract.SubjectGet(textFile.substring(min(500, textFile.length()-1)));
                            }
                            catch(Exception ex2)
                            {
                                ex2.printStackTrace();
                            }
                        }
                    }  
                } 
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("MetaData for " + fileName + " : ");
            metaDataObject.printMetadata();
            System.out.println(corePath);
            ProcessBuilder Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/*.xml");
            Process p = Process.start();
            p.waitFor();
            Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/*-text.txt");
            p = Process.start();
            p.waitFor();
            Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/*-meta.txt");
            p = Process.start();
            p.waitFor();
            Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/outputsplit.pdf");
            p = Process.start();
            p.waitFor();
            if (metaDataObject != null) {
                XMLCreate.XMLMaker(metaDataObject, corePath + "/" + fileName);
            }
        }
    }

    public MetaDataItem getMetaDataItem() {
        return metaDataObject;
    }

    private void cleaningBibTex(ArrayList<BibTexData> temp, String CheckString) throws IOException {
        boolean foundTitle = false;
        for (BibTexData btd : temp) {
            for (BibTexPair bp : btd.pairs) {
                if (bp.getField().equals("title")) {
                    String[] lines = CheckString.split("\\n");
                    for (String line : lines) {
                        String currentLine = line;
                        currentLine = currentLine.replaceAll("\\s+", "");
                        if (currentLine.length() > 5) {
                            String bpgetValue = bp.getValue();
                            bpgetValue = bpgetValue.replaceAll("\\s+", "");

                            if (bpgetValue.toLowerCase().contains(currentLine.toLowerCase())) //tries to search for "ISBN"
                            {
                                System.out.println("i have verified bibtex");
                                foundTitle = true;
                                MetaDataItem updatedMetaData = new MetaDataItem();
                                metaDataObject.setType(btd.type.substring(1));
                                btd.pairs.stream().map((bptemp) -> {
                                    if (bptemp.getField().equals("title")) {
                                        metaDataObject.setTitle(bptemp.getValue());
                                    }
                                    return bptemp;
                                }).map((bptemp) -> {
                                    if (bptemp.getField().equals("author")) {
                                        metaDataObject.setAuthor(bptemp.getValue());
                                    }
                                    return bptemp;
                                }).map((bptemp) -> {
                                    if (bptemp.getField().equals("year")) {
                                        metaDataObject.setDateCreated(bptemp.getValue());
                                    }
                                    return bptemp;
                                }).map((bptemp) -> {
                                    if (bptemp.getField().equals("publisher")) {
                                        metaDataObject.setPublisher(bptemp.getValue());
                                    }
                                    return bptemp;
                                }).filter((bptemp) -> (bptemp.getField().equals("journal"))).forEach((bptemp) -> {
                                    metaDataObject.setSource(bptemp.getValue());
                                });

                                if (btd.type.substring(1).equalsIgnoreCase("book")) {
                                    try {
                                        json1 = EnhanceBibTex.fetchData(btd);
                                        if (json1 != null && json1 != NULL) {
                                            System.out.println("enhanced");
                                            ConvertToText.extractDataEnhanced(json1, updatedMetaData);
                                            new updateMetadataObject(updatedMetaData);
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    String[] lines2 = lines;
                                    int temp1 = 0;
                                    while (temp1 < 100) {
                                        String line1 = lines2[temp1];
                                        temp1++;
                                        line1 = line1.replaceAll("\\s+", "");
                                        if (line1.toLowerCase().contains("thesis")) {
                                            metaDataObject.setType("thesis");
                                            metaDataObject.setAdvisor(Supervisor.getSupervisor());
                                            break;
                                        }
                                    }
                                }
                                break;

                            }
                        }
                    }
                    if (foundTitle) {
                        System.out.println(" BibtexrecievedfromAbstract");
                        BibtexrecievedfromAbstract = true;
                        return;
                    }
                }

            }
        }
    }

}
