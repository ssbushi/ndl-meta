package metaExtract;

import java.io.*;
import java.util.regex.*;
import static metaExtract.MainPage.filePath;
import static metaExtract.MainPage.metaDataObject;
import org.apache.commons.io.FilenameUtils;
import org.json.*;

/**
 * Searches the given file for DOI using regex, throws exception of not found or
 * if doi resource is not available on the database
 */
public class DOIParser {


    public static JSONObject fetchDOIData() throws DOINotFoundException, IOException {
        String noExtensionfilePath = FilenameUtils.removeExtension(filePath);
        String DOI;
        String metaFile = noExtensionfilePath + "-meta.txt";
        //First check in meta file
        //if fail then check text file
        //NOTE:: Uses the first DOI matched... Should be changed to tolerate more results?       
        DOI = searchInFile(metaFile);
        if (DOI == null) {
            DOI = searchInString(MainPage.textFile);
            if (DOI == null) {
                throw new DOINotFoundException("Cant Find any DOI");
            }
        }
        //System.out.println(DOI);
        String request = "http://api.crossref.org/works/" + DOI.trim();
        JSONObject json = findISBNJSON.readJsonFromUrl(request);
        if (json.toString().contains("Resource not found.")) {
            throw new DOINotFoundException("Resource Not Found");
        }
        return json;
    }

    //funct to search for DOI using regex in given file
    //works line by line
    //returns matched substring

    private static String searchInFile(String FileName) {
        
        String doi = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(FileName));
            for (String line; (line = br.readLine()) != null;) {
                String pattern = "10\\.[0-9]{4,}\\/[^\\s]*";
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(line);
                if (m.find()) {
                    doi = m.group();
                    break;
                }
            }
            br.close();
        } catch (Exception e) { }
        return doi;
    }

    private static String searchInString(String in) {
        String doi = null;
//        String[] lines = in.split("\\n");
//        for(String line: lines )
//        {
        String pattern = "10\\.[0-9]{4,}\\/[^\\s]*";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(MainPage.textFile);
        if (m.find()) {
            doi = m.group();
        }
        System.out.println("DOI : "+doi);
        metaDataObject.setDOI(doi);
        return doi;
    }
}
