package metaExtract;

import java.io.*;
import java.util.*;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.PdfUtilities;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.NonSequentialPDFParser;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.Splitter;

public class PDFTools {

    public static PDDocument readyPDF(String fileName) throws IOException
    {
        COSDocument cosDoc;
        File file = new File(fileName);
        try{
            PDFParser parser = new PDFParser(new FileInputStream(file));
            parser.parse();
            cosDoc = parser.getDocument();
        }
        catch(IOException e)
        {
            try{
                NonSequentialPDFParser parser = new NonSequentialPDFParser(new FileInputStream(file));
                parser.parse();
                cosDoc = parser.getDocument();
            }
            catch(IOException x)
            {
                throw x;
            }
        }
         return new PDDocument(cosDoc);
    }
//    public static String getText(String absFilePath) throws IOException {
//        PDFTextStripper pdfStripper = null;
//        File file = new File(absFilePath);
//        try{
//            PDFParser parser = new PDFParser(new FileInputStream(file));
//            parser.parse();
//            cosDoc = parser.getDocument();
//        }
//        catch(IOException e)
//        {
//            try{
//                NonSequentialPDFParser parser = new NonSequentialPDFParser(new FileInputStream(file));
//                parser.parse();
//                cosDoc = parser.getDocument();
//            }
//            catch(IOException x)
//            {
//                throw x;
//            }
//        }        
//        pdfStripper = new PDFTextStripper();
//        pdDoc = new PDDocument(cosDoc);
//        pdfStripper.setStartPage(1);
//        pdfStripper.setEndPage(10);
//        String parsedText = pdfStripper.getText(pdDoc);
//        System.out.println(parsedText);
//        return parsedText;
//    }
//
//    public static int getNumberOfPages(PDDocument pdDoc) {
//        return pdDoc.getNumberOfPages();
//    }

    public static void splitThis(String output, int start, int end) throws IOException 
    {
//        Splitter butcher = new Splitter();
//        butcher.setStartPage(start);
//        butcher.setEndPage(end);
//        butcher.setSplitAtPage(end-start+1);
//        List<PDDocument> listOfSplitPages;
//        listOfSplitPages = butcher.split(pdDoc);
//        Iterator<PDDocument> iterator = listOfSplitPages.listIterator();
//        System.out.println("Splitted!");
//        while(iterator.hasNext())
//        {
//            PDDocument doc=iterator.next();
//            System.out.println("Pages: "+doc.getNumberOfPages());
////            System.out.println("Text::::"+getText(doc));
//        }
        try
        {
            PdfUtilities.splitPdf(MainPage.filePath, output, String.valueOf(start), String.valueOf(end));
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }        
    }
    public static String getText(PDDocument doc) throws IOException {   
        PDFTextStripper pdfStripper = null;
        pdfStripper = new PDFTextStripper();
        String parsedText = pdfStripper.getText(doc);
        return parsedText;
    }

    public static String OCRPDF(String filePath,int start,int end) {
        String parent = filePath.substring(0,filePath.lastIndexOf("/"));
        PdfUtilities.splitPdf(filePath, parent+"/tess.pdf", String.valueOf(start), String.valueOf(end));
        File pdfFile = new File(parent+"/tess.pdf");
        ITesseract instance = new Tesseract(); 
        instance.setDatapath("/usr/share/tesseract-ocr");
        try {
            String result = instance.doOCR(pdfFile);
            //System.out.println(result);
            return result;
        } catch (TesseractException e) {
            System.err.println(e.getMessage());
        }
        finally
        {
            pdfFile.delete();
        }
        return null;
    }    
}