package metaExtract;

/**
 * XML file creator. Change according to usage.
 * Store Metadata in this format.
 * The data stored by this program looks like this:

    <?xml version="1.0" encoding="UTF-8" standalone="no" ?>
    <company>
    	<staff id="1">
    		<firstname>Biswarup</firstname>
    		<lastname>Bhattacharya</lastname>
    		<nickname>biswarupb</nickname>
    		<salary>15000</salary>
    	</staff>
    </company>

 * @author biswarupb
 * @version 05-Jun-2015
 */

import java.io.File;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.stream.StreamResult;
import metaExtract.MetaDataItem;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLCreate {

    public static void XMLMaker(MetaDataItem meta, String filename) {

      try {
          
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("dublin_core");
        doc.appendChild(rootElement);
        Attr attr1 = doc.createAttribute("schema");
        attr1.setValue("dc");
        rootElement.setAttributeNode(attr1);

        // staff elements

        List<String> newlist = null;
        if((meta.getPublisher() != null) && meta.getPublisher().length()>0 )
        {
            if(meta.getPublisher().contains(","))
                newlist = Arrays.asList((meta.getPublisher()).split(","));
            else if(meta.getPublisher().contains("+"))
                newlist = Arrays.asList((meta.getPublisher()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getPublisher()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("contributor");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("publisher");
                dc1.setAttributeNode(attr2);
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getEducationalLevel() != null) && meta.getEducationalLevel().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getEducationalLevel().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("educationlevel");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("none");
            dc1.setAttributeNode(attr2);
            rootElement.appendChild(dc1);
        }

        if((meta.getTypeOfLearningMaterial() != null) && meta.getTypeOfLearningMaterial().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getTypeOfLearningMaterial().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("type");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("typeoflearningmaterial");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getTypicalLearningTime() != null) && meta.getTypicalLearningTime().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getTypicalLearningTime().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("format");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("typicallearningtime");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getDifficultyLevel() != null))
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(("" + meta.getDifficultyLevel()).trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("format");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("difficultylevel");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getBoard() != null) && meta.getBoard().length()>0 )
        {
            if(meta.getBoard().contains(","))
                newlist = Arrays.asList((meta.getBoard()).split(","));
            else if(meta.getBoard().contains("+"))
                newlist = Arrays.asList((meta.getBoard()).split("\\+"));
            else if(meta.getBoard().contains(";"))
                newlist = Arrays.asList((meta.getBoard()).split(";"));
            else
                newlist = Arrays.asList((meta.getBoard()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("coverage");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("board");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }


        if((meta.getPrerequisiteTopic() != null) && meta.getPrerequisiteTopic().length()>0 )
        {
            if(meta.getPrerequisiteTopic().contains(","))
                newlist = Arrays.asList((meta.getPrerequisiteTopic()).split(","));
            else if(meta.getPrerequisiteTopic().contains("+"))
                newlist = Arrays.asList((meta.getPrerequisiteTopic()).split("\\+"));
            else if(meta.getPrerequisiteTopic().contains(";"))
                newlist = Arrays.asList((meta.getPrerequisiteTopic()).split(";"));
            else
                newlist = Arrays.asList((meta.getPrerequisiteTopic()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("subject");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("prerequisitetopic");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getPedagogicObjective() != null) && meta.getPedagogicObjective().length()>0 )
        {
            if(meta.getPedagogicObjective().contains("+"))
                newlist = Arrays.asList((meta.getPedagogicObjective()).split("\\+"));
            else if(meta.getPedagogicObjective().contains(";"))
                newlist = Arrays.asList((meta.getPedagogicObjective()).split(";"));
            else
                newlist = Arrays.asList((meta.getPedagogicObjective()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("subject");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("pedagogicobjective");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getResearcher() != null) && meta.getResearcher().length()>0 )
        {
            if(meta.getResearcher().contains(","))
                newlist = Arrays.asList((meta.getResearcher()).split(","));
            else if(meta.getResearcher().contains("+"))
                newlist = Arrays.asList((meta.getResearcher()).split("\\+"));
            else if(meta.getResearcher().contains(";"))
                newlist = Arrays.asList((meta.getResearcher()).split(";"));
            else if(meta.getResearcher().contains("/"))
                newlist = Arrays.asList((meta.getResearcher()).split("/"));
            else if(meta.getResearcher().toLowerCase().contains(" and "))
                newlist = Arrays.asList((meta.getResearcher().toLowerCase()).split(" and "));
            else
                newlist = Arrays.asList((meta.getResearcher()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("creator");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("researcher");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getKeyword() != null) && meta.getKeyword().length()>0 )
        {
            if(meta.getKeyword().contains(","))
                newlist = Arrays.asList((meta.getKeyword()).split(","));
            else if(meta.getKeyword().contains("+"))
                newlist = Arrays.asList((meta.getKeyword()).split("\\+"));
            else if(meta.getKeyword().contains(";"))
                newlist = Arrays.asList((meta.getKeyword()).split(";"));
            else if(meta.getKeyword().contains("/"))
                newlist = Arrays.asList((meta.getKeyword()).split("/"));
            else
                newlist = Arrays.asList((meta.getKeyword()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("subject");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("keyword");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getAdvisor() != null) && meta.getAdvisor().length()>0 )
        {
            if(meta.getAdvisor().contains(","))
                newlist = Arrays.asList((meta.getAdvisor()).split(","));
            else if(meta.getAdvisor().contains("+"))
                newlist = Arrays.asList((meta.getAdvisor()).split("\\+"));
            else if(meta.getAdvisor().contains(";"))
                newlist = Arrays.asList((meta.getAdvisor()).split(";"));
            else if(meta.getAdvisor().contains("/"))
                newlist = Arrays.asList((meta.getAdvisor()).split("/"));
            else if(meta.getAdvisor().toLowerCase().contains(" and "))
                newlist = Arrays.asList((meta.getAdvisor().toLowerCase()).split(" and "));
            else
                newlist = Arrays.asList((meta.getAdvisor()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("contributor");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("advisor");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getPlace() != null) && meta.getPlace().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getPlace()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("publisher");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("place");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getInstitution() != null) && meta.getInstitution().length()>0 )
        {
            if(meta.getInstitution().contains(","))
                newlist = Arrays.asList((meta.getInstitution()).split(","));
            else if(meta.getInstitution().contains("+"))
                newlist = Arrays.asList((meta.getInstitution()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getInstitution()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("publisher");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("institution");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getAwarded() != null) && meta.getAwarded().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getAwarded()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("date");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("awarded");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getDegree() != null) && meta.getDegree().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDegree().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("type");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("degree");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getDepartment() != null) && meta.getDepartment().length()>0 )
        {
            if(meta.getDepartment().contains(","))
                newlist = Arrays.asList((meta.getDepartment()).split(","));
            else if(meta.getDepartment().contains("+"))
                newlist = Arrays.asList((meta.getDepartment()).split("\\+"));
            else if(meta.getDepartment().contains(";"))
                newlist = Arrays.asList((meta.getDepartment()).split(";"));
            else
                newlist = Arrays.asList((meta.getDepartment()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("publisher");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("department");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getIsPartOf() != null) && meta.getIsPartOf().length()>0 )
        {
            if(meta.getIsPartOf().contains(","))
                newlist = Arrays.asList((meta.getIsPartOf()).split(","));
            else if(meta.getIsPartOf().contains("+"))
                newlist = Arrays.asList((meta.getIsPartOf()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getIsPartOf()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("relation");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("ispartof");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getIsPartOfSeries() != null) && meta.getIsPartOfSeries().length()>0 )
        {
            if(meta.getIsPartOfSeries().contains(","))
                newlist = Arrays.asList((meta.getIsPartOfSeries()).split(","));
            else if(meta.getIsPartOfSeries().contains("+"))
                newlist = Arrays.asList((meta.getIsPartOfSeries()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getIsPartOfSeries()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("relation");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("ispartofseries");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getHasPart() != null) && meta.getHasPart().length()>0 )
        {
            if(meta.getHasPart().contains(","))
                newlist = Arrays.asList((meta.getHasPart()).split(","));
            else if(meta.getHasPart().contains("+"))
                newlist = Arrays.asList((meta.getHasPart()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getHasPart()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("relation");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("haspart");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }
        
        if((meta.getIsReferencedBy() != null) && meta.getIsReferencedBy().length()>0 )
        {
            if(meta.getIsReferencedBy().contains(","))
                newlist = Arrays.asList((meta.getIsReferencedBy()).split(","));
            else if(meta.getIsReferencedBy().contains("+"))
                newlist = Arrays.asList((meta.getIsReferencedBy()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getIsReferencedBy()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("relation");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("isreferencedby");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getRefersTo() != null) && meta.getRefersTo().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getRefersTo()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("relation");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("refersto");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getRelatedResource() != null) && meta.getRelatedResource().length()>0 )
        {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(meta.getRelatedResource()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("relation");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("relatedresource");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
        }

        if((meta.getRightsURI() != null) && meta.getRightsURI().length()>0 )
        {
            if(meta.getRightsURI().contains(","))
                newlist = Arrays.asList((meta.getRightsURI()).split(","));
            else
                newlist = Arrays.asList((meta.getRightsURI()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("rights");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("uri");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getRightsHolder() != null) && meta.getRightsHolder().length()>0 )
        {
            if(meta.getRightsHolder().contains(","))
                newlist = Arrays.asList((meta.getRightsHolder()).split(","));
            else if(meta.getRightsHolder().contains("+"))
                newlist = Arrays.asList((meta.getRightsHolder()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getRightsHolder()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("rights");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("rightsholder");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getLicense() != null) && meta.getLicense().length()>0 )
        {
            if(meta.getLicense().contains(","))
                newlist = Arrays.asList((meta.getLicense()).split(","));
            else if(meta.getLicense().contains("+"))
                newlist = Arrays.asList((meta.getLicense()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getLicense()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("license");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("none");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getAuthor() != null) && meta.getAuthor().length()>0 )
        {
            if(meta.getAuthor().contains(","))
                newlist = Arrays.asList((meta.getAuthor()).split(","));
            else if(meta.getAuthor().contains("+"))
                newlist = Arrays.asList((meta.getAuthor()).split("\\+"));
            else if(meta.getAuthor().contains(";"))
                newlist = Arrays.asList((meta.getAuthor()).split(";"));
            else if(meta.getAuthor().contains("/"))
                newlist = Arrays.asList((meta.getAuthor()).split("/"));
            else if(meta.getAuthor().toLowerCase().contains(" and "))
                newlist = Arrays.asList((meta.getAuthor().toLowerCase()).split(" and "));
            else
                newlist = Arrays.asList((meta.getAuthor()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("contributor");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("author");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getCreator() != null) && meta.getCreator().length()>0 )
        {
            if(meta.getCreator().contains(","))
                newlist = Arrays.asList((meta.getCreator()).split(","));
            else if(meta.getCreator().contains("+"))
                newlist = Arrays.asList((meta.getCreator()).split("\\+"));
            else if(meta.getCreator().contains(";"))
                newlist = Arrays.asList((meta.getCreator()).split(";"));
            else if(meta.getCreator().toLowerCase().contains("and"))
                newlist = Arrays.asList((meta.getCreator().toLowerCase()).split("and"));
            else
                newlist = Arrays.asList((meta.getCreator()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("creator");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("none");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getEditor() != null) && meta.getEditor().length()>0 )
        {
            if(meta.getEditor().contains(","))
                newlist = Arrays.asList((meta.getEditor()).split(","));
            else if(meta.getEditor().contains("+"))
                newlist = Arrays.asList((meta.getEditor()).split("\\+"));
            else if(meta.getEditor().contains(";"))
                newlist = Arrays.asList((meta.getEditor()).split(";"));
            else if(meta.getEditor().toLowerCase().contains(" and "))
                newlist = Arrays.asList((meta.getEditor().toLowerCase()).split(" and "));
            else
                newlist = Arrays.asList((meta.getEditor()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("contributor");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("editor");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }
        
        if((meta.getIllustrator() != null) && meta.getIllustrator().length()>0 )
        {
            if(meta.getIllustrator().contains(","))
                newlist = Arrays.asList((meta.getIllustrator()).split(","));
            else if(meta.getIllustrator().contains("+"))
                newlist = Arrays.asList((meta.getIllustrator()).split("\\+"));
            else if(meta.getIllustrator().contains(";"))
                newlist = Arrays.asList((meta.getIllustrator()).split(";"));
            else if(meta.getIllustrator().contains("/"))
                newlist = Arrays.asList((meta.getIllustrator()).split("/"));
            else if(meta.getIllustrator().toLowerCase().contains(" and "))
                newlist = Arrays.asList((meta.getIllustrator().toLowerCase()).split(" and "));
            else
                newlist = Arrays.asList((meta.getIllustrator()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                String text = newlist.get(it).trim();
                String[] name = text.split("\\s+");
                if(name.length>1)
                {
                    text = name[name.length-1] + ", ";
                    for(int kk=0; kk<name.length-1; kk++)
                        text = text + name[kk] + " ";
                }
                text = text.replaceAll("and", "");
                dc1.appendChild(doc.createTextNode(text));
                Attr attr = doc.createAttribute("element");
                attr.setValue("contributor");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("editor");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getDateCreated() != null) && meta.getDateCreated().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDateCreated().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("date");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("created");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getDateAccessioned() != null) && meta.getDateAccessioned().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDateAccessioned().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("date");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("accessioned");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getDateCopyright() != null) && meta.getDateCopyright().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDateCopyright().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("date");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("copyright");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getUri() != null) && meta.getUri().length()>0 )
        {
            if(meta.getUri().contains(","))
                newlist = Arrays.asList((meta.getUri()).split(","));
            else
                newlist = Arrays.asList((meta.getUri()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("identifier");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("uri");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getISBN() != null) && meta.getISBN().length()>0 )
        {
            if(meta.getISBN().contains(","))
                newlist = Arrays.asList((meta.getISBN()).split(","));
            else if(meta.getISBN().contains("+"))
                newlist = Arrays.asList((meta.getISBN()).split("\\+"));
            else if(meta.getISBN().contains(";"))
                newlist = Arrays.asList((meta.getISBN()).split(";"));
            else if(meta.getISBN().contains("/"))
                newlist = Arrays.asList((meta.getISBN()).split("/"));
            else if(meta.getISBN().toLowerCase().contains("and"))
                newlist = Arrays.asList((meta.getISBN().toLowerCase()).split("and"));
            else
                newlist = Arrays.asList((meta.getISBN()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("identifier");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("isbn");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getISSN() != null) && meta.getISSN().length()>0 )
        {
            if(meta.getISSN().contains(","))
                newlist = Arrays.asList((meta.getISSN()).split(","));
            else if(meta.getISSN().contains("+"))
                newlist = Arrays.asList((meta.getISSN()).split("\\+"));
            else if(meta.getISSN().contains(";"))
                newlist = Arrays.asList((meta.getISSN()).split(";"));
            else if(meta.getISSN().contains("/"))
                newlist = Arrays.asList((meta.getISSN()).split("/"));
            else if(meta.getISSN().toLowerCase().contains("and"))
                newlist = Arrays.asList((meta.getISSN().toLowerCase()).split("and"));
            else
                newlist = Arrays.asList((meta.getISSN()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("identifier");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("issn");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getDOI() != null) && meta.getDOI().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDOI().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("identifier");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("doi");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getCitation() != null) && meta.getCitation().length()>0 )
        {
            if(meta.getCitation().contains(","))
                newlist = Arrays.asList((meta.getCitation()).split(","));
            else if(meta.getCitation().contains("+"))
                newlist = Arrays.asList((meta.getCitation()).split("\\+"));
            else if(meta.getCitation().contains(";"))
                newlist = Arrays.asList((meta.getCitation()).split(";"));
            else
                newlist = Arrays.asList((meta.getCitation()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("identifier");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("citation");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getDescriptionAbstract() != null) && meta.getDescriptionAbstract().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getDescriptionAbstract().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("description");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("abstract");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getTableOfContents() != null) && meta.getTableOfContents().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getTableOfContents().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("description");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("toc");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getLanguageISO() != null) && meta.getLanguageISO().length()>0 )
        {
            if(meta.getLanguageISO().contains(","))
                newlist = Arrays.asList((meta.getLanguageISO()).split(","));
            else if(meta.getLanguageISO().contains("+"))
                newlist = Arrays.asList((meta.getLanguageISO()).split("\\+"));
            else if(meta.getLanguageISO().contains(";"))
                newlist = Arrays.asList((meta.getLanguageISO()).split(";"));
            else if(meta.getLanguageISO().contains("/"))
                newlist = Arrays.asList((meta.getLanguageISO()).split("/"));
            else
                newlist = Arrays.asList((meta.getLanguageISO()).split("&"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("language");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("iso");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getSubject() != null) && meta.getSubject().length()>0 )
        {
            if(meta.getSubject().contains(","))
                newlist = Arrays.asList((meta.getSubject()).split(","));
            else if(meta.getSubject().contains("+"))
                newlist = Arrays.asList((meta.getSubject()).split("\\+"));
            else if(meta.getSubject().contains(";"))
                newlist = Arrays.asList((meta.getSubject()).split(";"));
            else
                newlist = Arrays.asList((meta.getSubject()).split("/"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("subject");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("none");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        if((meta.getTitle() != null) && meta.getTitle().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getTitle().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("title");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("none");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getType() != null) && meta.getType().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getType().trim().toLowerCase()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("type");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("none");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if(meta.getNumberOfPages() != 0)
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(String.valueOf(meta.getNumberOfPages())));
            Attr attr = doc.createAttribute("element");
            attr.setValue("numberofpages");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("none");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getMimeType() != null) && meta.getMimeType().length()>0 )
        {
            Element dc1 = doc.createElement("dcvalue");
            dc1.appendChild(doc.createTextNode(meta.getMimeType().trim()));
            Attr attr = doc.createAttribute("element");
            attr.setValue("format");
            dc1.setAttributeNode(attr);
            Attr attr2 = doc.createAttribute("qualifier");
            attr2.setValue("mimetype");
            dc1.setAttributeNode(attr2);            
            rootElement.appendChild(dc1);
        }

        if((meta.getSource() != null) && meta.getSource().length()>0 )
        {
            if(meta.getSource().contains(","))
                newlist = Arrays.asList((meta.getSource()).split(","));
            else if(meta.getSource().contains("+"))
                newlist = Arrays.asList((meta.getSource()).split("\\+"));
            else
                newlist = Arrays.asList((meta.getSource()).split(";"));
            for(int it=0; it<newlist.size(); it++)
            {
                Element dc1 = doc.createElement("dcvalue");
                dc1.appendChild(doc.createTextNode(newlist.get(it).trim()));
                Attr attr = doc.createAttribute("element");
                attr.setValue("source");
                dc1.setAttributeNode(attr);
                Attr attr2 = doc.createAttribute("qualifier");
                attr2.setValue("none");
                dc1.setAttributeNode(attr2);            
                rootElement.appendChild(dc1);
            }
        }

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(MainPage.filePath+".xml"));

        // Output to console for testing
        // StreamResult result = new StreamResult(System.out);

        transformer.transform(source, result);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        System.out.println("XML File saved!");

      } catch (ParserConfigurationException pce) {
        pce.printStackTrace();
      } catch (TransformerException tfe) {
        tfe.printStackTrace();
      } catch (Exception e) {
          e.printStackTrace();
      }
      
    }
}
