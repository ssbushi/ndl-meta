package metaExtract;

/**
 *Exception used by the 
 * DOIParser class
 * Useful for exception handling
 */
public class DOINotFoundException extends Exception {
    public DOINotFoundException(String message) {
        super(message);
    }
}