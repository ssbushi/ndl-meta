package metaExtract;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static metaExtract.MainPage.cwd;
import static metaExtract.MainPage.filePath;
import static metaExtract.MainPage.metaDataObject;
import org.apache.poi.hslf.HSLFSlideShow;
import org.apache.poi.hslf.model.*;
import org.apache.poi.hslf.usermodel.RichTextRun;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xslf.usermodel.*;

/**
 *
 * @author akash
 */
public class PPTParser {

    public static List<Triple<String, Integer, Integer>> detect(String text) throws Exception {
        String serializedClassifier = cwd + "/lib/english.muc.7class.distsim.crf.ser.gz";
        AbstractSequenceClassifier< CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);
        List< Triple< String, Integer, Integer>> triples = classifier.classifyToCharacterOffsets(text);
        return triples;
    }

    public static void parsePPTX(MetaDataItem metaData) {
        try {
            File f = new File(filePath);
            FileInputStream fis = new FileInputStream(f);
            XMLSlideShow show = new XMLSlideShow(fis);
            XSLFSlide slide[] = show.getSlides();
            metaDataObject.setNumberOfPages(slide.length);
            double max = 0;
            String title = "";
            String date = "";
            List<Triple<String, Integer, Integer>> text;
            for (int i = 0; i < 1; i++) {

                XSLFShape[] shape = slide[i].getShapes();
                for (XSLFShape shape1 : shape) {
                    if (shape1 instanceof XSLFTextShape) {
                        XSLFTextShape txt = (XSLFTextShape) shape1;
                        List<XSLFTextParagraph> p = txt.getTextParagraphs();
                        for (XSLFTextParagraph p1 : p) {
                            String a = p1.getText();
                            List<XSLFTextRun> run = p1.getTextRuns();
                            for (XSLFTextRun run1 : run) {
                                if (max < run1.getFontSize()) {
                                    max = run1.getFontSize();
                                    title = a;
                                }
                            }
                        }
                    }
                }
            }
            //System.out.println("Title: " + title);
            if (metaDataObject.getTitle().length() == 0 || metaDataObject.getTitle() == null) {
                metaDataObject.setTitle(title);
            }
            boolean flagAuthor = false;
            boolean flagCreator = false;
            boolean flagDate = false;
            for (int i = 0; i < 2; i++) {
                XSLFShape[] shape = slide[i].getShapes();
                for (XSLFShape shape1 : shape) {
                    if (shape1 instanceof XSLFTextShape) {
                        XSLFTextShape txt = (XSLFTextShape) shape1;
                        List<XSLFTextParagraph> p = txt.getTextParagraphs();
                        for (XSLFTextParagraph p1 : p) {
                            String parsedText = p1.getText();
                            text = detect(parsedText);
                            for (Triple<String, Integer, Integer> trip : text) {

                                if (trip.first.equalsIgnoreCase("Person") && !flagAuthor) {
                                    //System.out.println("Author: " + parsedText.substring(trip.second, trip.third));
                                    metaDataObject.setAuthor(parsedText.substring(trip.second, trip.third));
                                    flagAuthor = true;
                                    break;
                                } else if (trip.first.equalsIgnoreCase("Organization") && !flagCreator) {
                                    //System.out.println("Creator: " + parsedText.substring(trip.second, trip.third));
                                    if (metaDataObject.getCreator().length() == 0 || metaDataObject.getCreator() == null) {
                                        metaDataObject.setCreator(parsedText.substring(trip.second, trip.third));
                                    }
                                    flagCreator = true;
                                } else if (trip.first.equalsIgnoreCase("Date") && !flagDate) {
                                    date += " " + parsedText.substring(trip.second, trip.third);
                                }
                            }
                            if (!date.equals("") && !flagDate) {
                                //System.out.println("Date of Creation: " + date);
                                if(metaDataObject.getDateCreated().length() == 0 || metaDataObject.getDateCreated() == null) {
                                    metaDataObject.setDateCreated(date);
                                }
                                flagDate = true;
                            }
                            if (flagAuthor && flagCreator && flagDate) {
                                break;
                            }
                        }
                        if (flagAuthor && flagCreator && flagDate) {
                            break;
                        }
                    }
                    if (flagAuthor && flagCreator && flagDate) {
                        break;
                    }
                }
                if (flagAuthor && flagCreator && flagDate) {
                    break;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException ex) {
            Logger.getLogger(PPTParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PPTParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void parsePPT(MetaDataItem metaData) {
        try {
            boolean flagAuthor = false;
            boolean flagCreator = false;
            boolean flagDate = false;
            boolean flagSubject = false;
            File f = new File(filePath);
            FileInputStream fis = new FileInputStream(f);
            POIFSFileSystem fs = new POIFSFileSystem(fis);
            HSLFSlideShow show = new HSLFSlideShow(fs);
            SlideShow ss = new SlideShow(show);
            Slide[] slides = ss.getSlides();
            metaDataObject.setNumberOfPages(slides.length);
            List<Triple<String, Integer, Integer>> text;
            int max = 0;
            String title = "";
            String date = "";
            for (int x = 0; x < 1; x++) {
                TextRun[] runs = slides[x].getTextRuns();
                for (TextRun run : runs) {
                    RichTextRun richRun = run.getRichTextRunAt(0);
                    if (richRun.getFontSize() > max) {
                        max = richRun.getFontSize();
                        title = run.getText();
                    }
                }
            }
            for (int x = 0; x < 1; x++) {
                Shape[] shapes = slides[x].getShapes();
                for (Shape shape : shapes) {
                    if (shape instanceof TextShape) {
                        TextShape textShape = (TextShape) shape;
                        TextRun run = textShape.getTextRun();
                        RichTextRun richRun = run.getRichTextRunAt(0);
                        if (richRun.getFontSize() > max) {
                            max = richRun.getFontSize();
                            title = run.getText();
                        }
                    }
                }

            }
            if (metaDataObject.getTitle().length() == 0 || metaDataObject.getTitle() == null) {
                metaDataObject.setTitle(title);
            }
            System.out.println("Title: "+title);
            for (int x = 0; x < 2; x++) {
                HeadersFooters headFoot = slides[x].getHeadersFooters();
                String header = headFoot.getHeaderText();
                if (header == null) {
                    header = "";
                }
                text = detect(header);
                for (Triple<String, Integer, Integer> trip : text) {

                    if (trip.first.equalsIgnoreCase("Person") && !flagAuthor) {
                        //System.out.println("Author: " + header.substring(trip.second, trip.third));
                        metaDataObject.setAuthor(header.substring(trip.second, trip.third));
                        flagAuthor = true;
                    } else if (trip.first.equalsIgnoreCase("Organization") && !flagCreator) {
                        //System.out.println("Creator: " + header.substring(trip.second, trip.third));
                        if (metaDataObject.getCreator().length() == 0 || metaDataObject.getCreator() == null) {
                            metaDataObject.setCreator(header.substring(trip.second, trip.third));
                        }
                        flagCreator = true;
                    } else if (trip.first.equalsIgnoreCase("Date") && !flagDate) {
                        date += " " + header.substring(trip.second, trip.third);
                    }

                }
                if (!date.equals("") && !flagDate) {
                    //System.out.println("Date of Creation: " + date);
                    if (metaDataObject.getDateCreated().length() == 0 || metaDataObject.getDateCreated() == null) {
                        metaDataObject.setDateCreated(date);
                    }
                    flagDate = true;
                }
                if (!flagAuthor && !flagCreator && !flagDate) {

                    if (!flagSubject && !header.equals("")) {
                        //System.out.println("Subject (probable): " + header);
                        if (metaDataObject.getSubject().length() == 0 || metaDataObject.getSubject() == null) {
                            metaDataObject.setSubject(header);
                        }
                        flagSubject = true;
                    }
                }
                if (flagAuthor && flagCreator && flagDate && flagSubject) {
                    break;
                }
                String footer = headFoot.getFooterText();
                if (footer == null) {
                    footer = "";
                }
                text = detect(footer);
                for (Triple<String, Integer, Integer> trip : text) {
                    if (trip.first.equalsIgnoreCase("Person") && !flagAuthor) {
                        //System.out.println("Author: " + footer.substring(trip.second, trip.third));
                        metaDataObject.setAuthor(footer.substring(trip.second, trip.third));
                        flagAuthor = true;
                    } else if (trip.first.equalsIgnoreCase("Organization") && !flagCreator) {
                        //System.out.println("Creator: " + footer.substring(trip.second, trip.third));
                        if (metaDataObject.getCreator().length() == 0 || metaDataObject.getCreator() == null) {
                            metaDataObject.setCreator(footer.substring(trip.second, trip.third));
                        }
                        flagCreator = true;
                    } else if (trip.first.equalsIgnoreCase("Date") && !flagDate) {
                        date += " " + footer.substring(trip.second, trip.third);
                    }
                }
                if (!date.equals("") && !flagDate) {
                    //System.out.println("Date of Creation: " + date);
                    if (metaDataObject.getDateCreated().length() == 0 || metaDataObject.getDateCreated() == null) {
                        metaDataObject.setDateCreated(date);
                    }
                    flagDate = true;
                }
                if (!flagAuthor && !flagCreator && !flagDate) {
                    if (!flagSubject && !footer.equals("")) {
                        //System.out.println("Subject (probable): " + footer);
                        metaData.setSubject(footer);
                        flagSubject = true;
                    }
                }
                if (flagAuthor && flagCreator && flagDate && flagSubject) {
                    break;
                }
                TextRun[] runs = slides[x].getTextRuns();
                for (TextRun run : runs) {
                    text = detect(run.getText());
                    for (Triple<String, Integer, Integer> trip : text) {

                        if (trip.first.equalsIgnoreCase("Person") && !flagAuthor) {
                            // System.out.println("Author: " + run.getText().substring(trip.second, trip.third));
                            metaDataObject.setAuthor(run.getText().substring(trip.second, trip.third));
                            flagAuthor = true;
                            break;
                        } else if (trip.first.equalsIgnoreCase("Organization") && !flagCreator) {
                            //System.out.println("Creator: " + footer.substring(trip.second, trip.third));
                            if (metaDataObject.getCreator().length() == 0 || metaDataObject.getCreator() == null) {
                                metaDataObject.setCreator(run.getText().substring(trip.second, trip.third));
                            }
                            flagCreator = true;
                        } else if (trip.first.equalsIgnoreCase("Date") && !flagDate) {
                            date += " " + run.getText().substring(trip.second, trip.third);
                        }
                    }
                    if (!date.equals("") && !flagDate) {
                        //System.out.println("Date of Creation: " + date);
                        if (metaDataObject.getDateCreated().length() == 0 || metaDataObject.getDateCreated() == null) {
                            metaDataObject.setDateCreated(date);
                        }
                        flagDate = true;
                    }
                    if (flagAuthor && flagCreator && flagDate) {
                        break;
                    }
                }
                if (flagAuthor && flagCreator && flagDate && flagSubject) {
                    break;
                }
                Shape[] shapes = slides[x].getShapes();
                for (Shape shape : shapes) {
                    if (shape instanceof TextShape) {
                        TextShape textShape = (TextShape) shape;
                        TextRun run = textShape.getTextRun();
                        text = detect(run.getText());
                        for (Triple<String, Integer, Integer> trip : text) {

                            if (trip.first.equalsIgnoreCase("Person") && !flagAuthor) {
                                //System.out.println("Author: " + run.getText().substring(trip.second, trip.third));
                                metaDataObject.setAuthor(run.getText().substring(trip.second, trip.third));
                                flagAuthor = true;
                                break;
                            } else if (trip.first.equalsIgnoreCase("Organization") && !flagCreator) {
                                //System.out.println("Creator: " + footer.substring(trip.second, trip.third));
                                if (metaDataObject.getCreator().length() == 0 || metaDataObject.getCreator() == null) {
                                    metaDataObject.setCreator(run.getText().substring(trip.second, trip.third));
                                }
                                flagCreator = true;
                            } else if (trip.first.equalsIgnoreCase("Date") && !flagDate) {
                                date += " " + run.getText().substring(trip.second, trip.third);
                            }
                        }
                        if (!date.equals("") && !flagDate) {
                            //System.out.println("Date of Creation: " + date);
                            if (metaDataObject.getDateCreated().length() == 0 || metaDataObject.getDateCreated() == null) {
                                metaDataObject.setDateCreated(date);
                            }
                            flagDate = true;
                        }
                    }
                    if (flagAuthor && flagCreator && flagDate) {
                        break;
                    }
                }
                if (flagAuthor && flagCreator && flagDate) {
                    break;
                }
            }
        } catch (IOException ioe) {
        } catch (Exception ex) {
            Logger.getLogger(PPTParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
