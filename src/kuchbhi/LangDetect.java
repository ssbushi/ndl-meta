package metaExtract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;
import static metaExtract.MainPage.metaDataObject;
import org.json.JSONArray;
import org.json.JSONObject;
/**
 *
 * @author biswarupb
 */
public class LangDetect {
    
    public static void LangGet(String text, String key)
    {
        try {
            String url = "http://ws.detectlanguage.com/0.2/detect?q[]="+text+"&key=" + key;
            url = url.replaceAll(" ", "%20");
            JSONObject json1 = new JSONObject(getUrlSource(url));
            JSONObject json2 = json1.getJSONObject("data");
            JSONArray json3 = json2.getJSONArray("detections");
            String lang = "";
            JSONArray json5 = json3.getJSONArray(0);
            for(int i=0; i<json3.length(); i++)
            {
                JSONObject json4 = json5.getJSONObject(i);
                if(i==0)
                    lang = (String) json4.get("language");
                else
                    lang += ", " + (String) json4.get("language");
                
            }
            System.out.println(lang);
            if(metaDataObject.getLanguageISO() == null || "".equals(metaDataObject.getLanguageISO()))
                    metaDataObject.setLanguageISO(lang);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String getUrlSource(String url) throws IOException
    {
        URL yahoo = new URL(url);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.3.100.207", 8080));
        HttpURLConnection yc = (HttpURLConnection) yahoo.openConnection(proxy);
//        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
        BufferedReader reader = new BufferedReader(new InputStreamReader(((HttpURLConnection) (new URL(url)).openConnection()).getInputStream(), Charset.forName("UTF-8")));
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = reader.readLine()) != null)
            a.append(inputLine);
        reader.close();
        return a.toString();
    }
}
