package metaExtract;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.util.Triple;
import java.io.FileInputStream;
import java.util.List;
import static metaExtract.MainPage.cwd;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hwpf.usermodel.Paragraph;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.*;




/* Takes a file path and extracts author, organization,Location.
Since it is possible that many people and places will be mentioned in the same document,
The following protocol willbe applied to decide on which to choose.
The prefference order is, the information in the footer is considered most reliable, then the header,
and then the first occurence in the main text.
The title is the text that appears in the largest font size among the first 20 paragraphs.
*/
public class DocDetect {
    public static List<Triple<String,Integer,Integer>> detect(String text) throws Exception{
        String serializedClassifier =cwd+"/lib/english.muc.7class.distsim.crf.ser.gz";
        AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);
        List<Triple<String,Integer,Integer>> triples = classifier.classifyToCharacterOffsets(text);
        return triples;
    }
    public static void parseDoc(String filePath, MetaDataItem metaData) throws Exception{
        HWPFDocument docx = new HWPFDocument(
        new FileInputStream(filePath));
        //using XWPFWordExtractor Class
        WordExtractor we = new WordExtractor(docx);
        List<Triple<String,Integer,Integer>> footer,header,body;
        boolean flagAuthor=false, flagCreator=false, flagLocation=false,flagDate=false,flagTitle=false,headerDataFound=false;
        String author = "",creator = "",date="",location="",title="";
        if(we.getDocSummaryInformation().getCompany()!=null)
        {
            flagCreator=true;
            creator=we.getDocSummaryInformation().getCompany();
        }
        if(we.getDocSummaryInformation().getSlideCount()!=0)
        {
            metaData.setNumberOfPages(we.getDocSummaryInformation().getSlideCount());
        }
        try
        {
            
            footer=detect(we.getFooterText());
            for(Triple<String,Integer,Integer> trip:footer)
            {
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    author=we.getFooterText().substring(trip.second, trip.third);
                    //System.out.println("Author [from footer]="+we.getFooterText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    creator=we.getFooterText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from footer]="+we.getFooterText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    location=we.getFooterText().substring(trip.second, trip.third);
                    //System.out.println("Location [from footer]="+we.getFooterText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    date=we.getFooterText().substring(trip.second, trip.third);
                    //System.out.println("Date [from footer]="+we.getFooterText().substring(trip.second, trip.third));
                }
            }
            if((!flagAuthor)&&(!flagCreator)&&(!flagLocation)&&(!flagDate)&&(we.getFooterText().length()>2))
            {
                flagTitle=true;
                title=we.getFooterText();
                //System.out.println("Title="+title);
            }
            
        }
        catch(Exception e)
        {
            System.out.println("Header not found");
        }
        try
        {
            header=detect(we.getHeaderText());
            for(Triple<String,Integer,Integer> trip:header)
            {
            
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    author=we.getHeaderText().substring(trip.second, trip.third);
                    //System.out.println("Author [from header]="+we.getHeaderText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    creator=we.getHeaderText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from header]="+we.getHeaderText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    location=we.getHeaderText().substring(trip.second, trip.third);
                    //System.out.println("Location [from header]="+we.getHeaderText().substring(trip.second, trip.third));
                }else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    date=we.getHeaderText().substring(trip.second, trip.third);
                    //System.out.println("Date [from Header]="+we.getHeaderText().substring(trip.second, trip.third));
                }
            }
            if((!headerDataFound)&&(we.getHeaderText().length()>2))
            {
                flagTitle=true;
                title=we.getHeaderText();
                //System.out.println("Title="+title);
            }
        }
        catch(Exception e)
        {
            //System.out.println("Header not found");
        }
        
        try
        {
            body=detect(we.getText());
            //System.out.println(body.size());
        
            
            for(Triple<String,Integer,Integer> trip:body)
            {
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    author=we.getText().substring(trip.second, trip.third);
                    String sub = we.getText().substring(trip.third,100+trip.third);
                    List<Triple<String,Integer,Integer>> body1 = detect(sub);
                    for(Triple<String,Integer,Integer> trip1:body1){
                        if(trip.first.equalsIgnoreCase("Person")){
                            author += ", "+sub.substring(trip1.second, trip1.third);
                        }

                    }
                    //System.out.println("Author [from body]="+we.getText().substring(trip.second(), trip.third()));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    creator=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from body]="+we.getText().substring(trip.second(), trip.third()));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    location=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Location [from body]="+we.getText().substring(trip.second(), trip.third()));
                }else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    date=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Date [from Body]="+we.getText().substring(trip.second, trip.third));
                }
            
            }
        }
        catch(Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Body Not Found");
        }
        if(!flagTitle)
        {
            int maxsize=-10;
            int maxp=-1;
            Range r = docx.getRange();
            for(int j=0; j<Math.min(r.numParagraphs(),20); j++) {
                Paragraph p = r.getParagraph(j);
                String ptext = p.text();

                if((p.text().length()>2)&&(p.getCharacterRun(0).getFontSize()>maxsize))
                {
                    maxsize=p.getCharacterRun(0).getFontSize();
                    maxp=j;
                }
            }
        //System.out.println(maxp);
            Paragraph p=r.getParagraph(maxp);
            title=p.text();
            flagTitle=true;
            //System.out.println("Probable Title- "+ p.text());
        }
        if(("".equals(metaData.getTitle()))&&flagTitle)
        {
            metaData.setTitle(title);
        }
        if(("".equals(metaData.getDateCreated()))&&flagDate)
        {
            metaData.setDateCreated(date);
        }
        if(("".equals(metaData.getAuthor()))&&flagAuthor)
        {
            metaData.setAuthor(author);
        }
        if(("".equals(metaData.getInstitution()))&&flagCreator)
        {
            metaData.setCreator(creator);
        }
    }
    public static void parseDocx(String filePath, MetaDataItem metaData) throws Exception{
        XWPFDocument docx = new XWPFDocument(
        new FileInputStream(filePath));
        //using XWPFWordExtractor Class
        XWPFWordExtractor we = new XWPFWordExtractor(docx);
        //System.out.println(we.getExtendedProperties().getCompany()+"   "+we.getExtendedProperties().getPages());
        List<Triple<String,Integer,Integer>> footer,header,body;
        footer=header=body=null;
        boolean flagAuthor=false, flagCreator=false, flagLocation=false,flagDate=false,flagTitle=false,headerDataFound=false;
        XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(docx);
        XWPFHeader head = policy.getDefaultHeader();
        XWPFFooter foot = policy.getDefaultFooter();
        String author = "",creator = "",location="",title="",date="";
        if(we.getExtendedProperties().getCompany()!=null)
        {
            flagCreator=true;
            creator=we.getExtendedProperties().getCompany();
        }
        if(we.getExtendedProperties().getPages()!=0)
        {
            metaData.setNumberOfPages(we.getExtendedProperties().getPages());
        }
        if(head==null)
            head=policy.getFirstPageHeader();
        if(foot==null)
            foot=policy.getFirstPageFooter();
        try{
            footer=detect(foot.getText());
            for(Triple<String,Integer,Integer> trip:footer)
            {
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    author=foot.getText().substring(trip.second, trip.third);
                    //System.out.println("Author [from footer]="+foot.getText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    creator=foot.getText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from footer]="+foot.getText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    location=foot.getText().substring(trip.second, trip.third);
                    //System.out.println("Location [from footer]="+foot.getText().substring(trip.second, trip.third));
                }else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    date=foot.getText().substring(trip.second, trip.third);
                    //System.out.println("Date [from footer]="+foot.getText().substring(trip.second, trip.third));
                }
            }
            if((!flagAuthor)&&(!flagCreator)&&(!flagLocation)&&(!flagDate)&&(foot.getText().length()>2))
            {
                flagTitle=true;
                title=foot.getText();
                //System.out.println("Title="+title);
            }
        }
        catch(Exception e)
        {
            //System.out.println("FOoter not found");
        }
        try
        {
            header=detect(head.getText());
            for(Triple<String,Integer,Integer> trip:header)
            {
            
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    headerDataFound=true;
                    author=head.getText().substring(trip.second, trip.third);
                    //System.out.println("Author [from header]="+head.getText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    headerDataFound=true;
                    creator=head.getText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from header]="+head.getText().substring(trip.second, trip.third));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    headerDataFound=true;
                    location=head.getText().substring(trip.second, trip.third);
                    //System.out.println("Location [from header]="+head.getText().substring(trip.second, trip.third));
                }else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    headerDataFound=true;
                    date=head.getText().substring(trip.second, trip.third);
                    //System.out.println("Date [from Header]="+head.getText().substring(trip.second, trip.third));
                }
            }
            if((!headerDataFound)&&(head.getText().length()>2))
            {
                flagTitle=true;
                title=head.getText();
                //System.out.println("Title="+title);
            }
        }
        catch(Exception e)
        {
            //System.out.println("Header Not Found");
        }
        try
        {
            body=detect(we.getText());
        
        
            for(Triple<String,Integer,Integer> trip:body)
            {
                if(trip.first.equalsIgnoreCase("Person")&&(!flagAuthor))
                {
                    flagAuthor=true;
                    author=we.getText().substring(trip.second(), trip.third());
                    String sub = we.getText().substring(trip.third,100+trip.third);
                    List<Triple<String,Integer,Integer>> body1 = detect(sub);
                    for(Triple<String,Integer,Integer> trip1:body1){
                        if(trip.first.equalsIgnoreCase("Person")){
                            author += ", "+sub.substring(trip1.second(), trip1.third());
                        }

                    }
                    //System.out.println("Author [from body]="+we.getText().substring(trip.second(), trip.third()));
                }
                else if(trip.first.equalsIgnoreCase("Organization")&&(!flagCreator))
                {
                    flagCreator=true;
                    creator=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Creator [from body]="+we.getText().substring(trip.second(), trip.third()));
                }
                else if(trip.first.equalsIgnoreCase("Location")&&(!flagLocation))
                {
                    flagLocation=true;
                    location=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Location [from body]="+we.getText().substring(trip.second(), trip.third()));
                }else if(trip.first.equalsIgnoreCase("Date")&&(!flagDate))
                {
                    flagDate=true;
                    date=we.getText().substring(trip.second, trip.third);
                    //System.out.println("Date [from Body]="+we.getText().substring(trip.second, trip.third));
                }
            
            }
        }
        catch(Exception e)
        {
            //e.printStackTrace();
            //System.out.println("No body");
        }
        if(!flagTitle)
        {
            int maxsize=-10;
            XWPFRun maxp=null;
            List<XWPFParagraph> paragraphList =  docx.getParagraphs();
            if(paragraphList==null)
                return;
            int cnt=0;
            for(XWPFParagraph p:paragraphList) {
                cnt++;
                for(XWPFRun run : p.getRuns()){
            //System.out.println("YO");
                //String ptext = p.getText();
                //CTR ctrun=CTR.Factory.newInstance();
                //XWPFRun xrun= new XWPFRun(ctrun,p);
                //System.out.println(xrun.);
                    if((p.getText().length()>2)&&(run.getFontSize()>maxsize))
                    {
                        maxsize=run.getFontSize();
                        maxp=run;
                    }
                }
                if(cnt==20)
                    break;
            }
            //System.out.println(maxp);
            XWPFRun r=maxp;
            if(r==null)
                return;
            title=r.toString();
            //System.out.println("Probable Title- "+ r.toString());
            flagTitle=true;
        }
        if(("".equals(metaData.getTitle()))&&flagTitle)
        {
            metaData.setTitle(title);
        }
        if(("".equals(metaData.getDateCreated()))&&flagDate)
        {
            metaData.setDateCreated(date);
        }
        if(("".equals(metaData.getAuthor()))&&flagAuthor)
        {
            metaData.setAuthor(author);
        }
        if(("".equals(metaData.getInstitution()))&&flagCreator)
        {
            metaData.setInstitution(creator);
        }
    }
    
}
