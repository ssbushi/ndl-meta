package metaExtract;

import java.io.*;
import static metaExtract.MainPage.filePath;
import static metaExtract.MainPage.metaDataObject;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.*;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.exception.TikaException;
import org.json.*;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ConvertToText {

    static int maxChar = 50000;//Maximum characters to parse[100000 is the limit in tika]

    public static void setMax(int newMax) {
        maxChar = newMax;
    }

    public static void convert(MetaDataItem metaData) throws Exception {

        Metadata metadata = new Metadata();
        PrintWriter writer = null;
        String writeContent;
        InputStream input = null;
        
        try {
                input = new FileInputStream(new File(filePath));
                String noExtension = FilenameUtils.removeExtension(filePath);//to remove ".pdf" etc
                writer = new PrintWriter(noExtension + "-meta.txt", "UTF-8");
                ContentHandler textHandler = new BodyContentHandler();
                Parser parser = new AutoDetectParser();
                ParseContext context = new ParseContext();
                try {
                    parser.parse(input, textHandler, metadata, context);
                } catch (Exception e) {
                    System.out.println("i also catch");
                    e.printStackTrace();
                    //It is there for convenience
                }
                writeContent = textHandler.toString();
                writeContent = writeContent.substring(0, Math.min(writeContent.length(), maxChar));
                /*Cant use ocr as we dont know the file type*/
                MainPage.textFile=writeContent;
            
            
            
            String[] metadataNames = metadata.names();
            System.out.println(metadata.names().length);
            for (String name : metadataNames) {
                System.out.println(" Item" +name);
                if ((name.equalsIgnoreCase("TITLE"))||(name.equalsIgnoreCase("dc:TITLE"))||(name.equalsIgnoreCase("meta:TITLE"))) {
                    metaData.setTitle(metadata.get(name));
                    writer.println("Title:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Description"))||(name.equalsIgnoreCase("dc:Description"))||(name.equalsIgnoreCase("meta:Description"))) {
                    metaData.setDescriptionAbstract(metadata.get(name));
                    writer.println("Description:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Language"))||(name.equalsIgnoreCase("dc:Language"))||(name.equalsIgnoreCase("meta:Language"))) {
                    metaData.setLanguageISO(metadata.get(name));
                    writer.println("Language:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Creator"))||(name.equalsIgnoreCase("dc:Creator"))||(name.equalsIgnoreCase("meta:Creator"))) {
                    metaData.setCreator(metadata.get(name));
                    writer.println("Creator:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Identifier"))||(name.equalsIgnoreCase("dc:Identifier"))||(name.equalsIgnoreCase("meta:Identifier"))) {
                    metaData.setUri(metadata.get(name));
                    writer.println("Identifier:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Subject"))||(name.equalsIgnoreCase("dc:Subject"))||(name.equalsIgnoreCase("meta:Subject"))) {
                    metaData.setSubject(metadata.get(name));
                    writer.println("Subject:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Rights"))||(name.equalsIgnoreCase("dc:Rights"))||(name.equalsIgnoreCase("meta:Rights"))) {
                    metaData.setDateCopyright(metadata.get(name));
                    writer.println("Rights:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Publisher"))||(name.equalsIgnoreCase("dc:Publisher"))||(name.equalsIgnoreCase("meta:Publisher"))) {
                    metaData.setEditor(metadata.get(name));//No field available for publisher
                    writer.println("Publisher:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("SOurce"))||(name.equalsIgnoreCase("dc:SOurce"))||(name.equalsIgnoreCase("meta:SOurce"))) {
                    metaData.setSource(metadata.get(name));
                    writer.println("SOurce:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Type"))||(name.equalsIgnoreCase("dc:Type"))||(name.equalsIgnoreCase("meta:Type"))) {
                    metaData.setType(metadata.get(name));
                    writer.println("Type:" + metadata.get(name));
                } else if ((name.equalsIgnoreCase("Created"))||(name.equalsIgnoreCase("dc:Created"))||(name.equalsIgnoreCase("meta:Creation-date"))){
                    metaData.setDateCreated(metadata.get(name));
                    writer.println("Created:" + metadata.get(name));

                }else if ((name.equalsIgnoreCase("Author"))||(name.equalsIgnoreCase("dc:Author"))||(name.equalsIgnoreCase("meta:Author"))){
                    metaData.setAuthor(metadata.get(name));
                    writer.println("Author:" + metadata.get(name));

                }else if ((name.equalsIgnoreCase("Page-Count"))||(name.equalsIgnoreCase("dc:SizeOrDuration"))||(name.equalsIgnoreCase("Slide-Count"))){
                    metaData.setNumberOfPages(Integer.parseInt(metadata.get(name)));
                    writer.println("NOP:" + metadata.get(name));

                }else if ((name.equalsIgnoreCase("Content-type"))||(name.equalsIgnoreCase("dc:format"))){
                    metaData.setMimeType(metadata.get(name));
                    writer.println("Mime:" + metadata.get(name));
                }
                System.out.println("MetaData : ");
                        metaData.printMetadata();
            }
        } catch (IOException | NumberFormatException e) { 
//            throw e; 
            e.printStackTrace();
        }
        finally{
            if(writer != null)
                writer.close();
            if(input != null)
                input.close();
        }

    }

    public static void extractData(JSONObject json1,MetaDataItem metaData) {
        
        JSONArray jarray = json1.getJSONArray("items");
        JSONObject json = jarray.getJSONObject(0);
        JSONObject json3 = json.getJSONObject("volumeInfo");
        
        try {
            String name = (String) json3.get("title");
            metaDataObject.setTitle(name);
        } catch (Exception e) {}
        
        try {
            String subname = (String) json3.get("subtitle");
            metaDataObject.setTitle(metaDataObject.getTitle() + " " + subname);
        } catch (Exception e) {}
        
        try {
            String abstrac = (String) json3.get("description");
            metaDataObject.setDescriptionAbstract(abstrac);
        } catch (Exception e) {}
        
        try {
            String language = (String) json3.get("language");
            metaDataObject.setLanguageISO(language);
        } catch (Exception e) {}
        
        try {
            int pagecnt = (int) json3.get("pageCount");
            metaData.setNumberOfPages(pagecnt);
        } catch (Exception e) {}
        
        try {
            String publisher = (String) json3.get("publisher");
            metaDataObject.setPublisher(publisher);
        } catch (Exception e) {}
        
        try {
            String type = (String) json3.get("printType");
            metaDataObject.setType(type);
        } catch (Exception e) {}
        
        try {
            String publishdate = (String) json3.get("publishedDate");
            metaDataObject.setDateCreated(publishdate);
        } catch (Exception e) {}
        
        try {
            JSONArray author = json3.getJSONArray("authors");
            String authors = new String();
            for (int i = 0; i < author.length(); i++) {
                authors = authors.concat(author.getString(i));
                authors = authors.concat(", ");
            }
            metaDataObject.setAuthor(authors);
        } catch (Exception e) {}
    }
    
    public static void extractDataEnhanced(JSONObject json,MetaDataItem metaData) {
        
        JSONObject json3 = json.getJSONObject("volumeInfo");
        
        try {
            String name = (String) json3.get("title");
            metaDataObject.setTitle(name);
        } catch (Exception e) {}
        
        try {
            String subname = (String) json3.get("subtitle");
            metaDataObject.setTitle(metaDataObject.getTitle() + " " + subname);
        } catch (Exception e) {}
        
        try {
            String abstrac = (String) json3.get("description");
            metaDataObject.setDescriptionAbstract(abstrac);
        } catch (Exception e) {}
        
        try {
            String language = (String) json3.get("language");
            metaDataObject.setLanguageISO(language);
        } catch (Exception e) {}
        
        try {
            int pagecnt = (int) json3.get("pageCount");
            metaData.setNumberOfPages(pagecnt);
        } catch (Exception e) {}
        
        try {
            String publisher = (String) json3.get("publisher");
            metaDataObject.setPublisher(publisher);
        } catch (Exception e) {}
        
        try {
            String type = (String) json3.get("printType");
            metaDataObject.setType(type);
        } catch (Exception e) {}
        
        try {
            String publishdate = (String) json3.get("publishedDate");
            metaDataObject.setDateCreated(publishdate);
        } catch (Exception e) {}
        
        try {
            JSONArray author = json3.getJSONArray("authors");
            String authors = new String();
            for (int i = 0; i < author.length(); i++) {
                authors = authors.concat(author.getString(i));
                authors = authors.concat(", ");
            }
            metaDataObject.setAuthor(authors);
        } catch (Exception e) {}
    }

    public static void extractDatafromDOIJSONObject(JSONObject json1, MetaDataItem metaData) {
        JSONObject jobject = json1.getJSONObject("message");
        JSONArray jarray;
        
        
        try {
            jarray = jobject.getJSONArray("title");
            String title = new String();
            for (int i = 0; i < jarray.length(); i++) {
                title = title.concat(jarray.getString(i));
                title = title.concat(" ");
            }
            metaDataObject.setTitle(title);
        } catch (Exception e) {}
        
        try {
            jarray = jobject.getJSONArray("subtitle");
            String subtitle = new String();
            for (int i = 0; i < jarray.length(); i++) {
                subtitle = subtitle.concat(jarray.getString(i));
                subtitle = subtitle.concat(" ");
            }
            metaDataObject.setTitle(metaDataObject.getTitle() + " " + subtitle);
        } catch (Exception e) {}

        try {
            jarray = jobject.getJSONArray("subject");
            String subject = new String();
            for (int i = 0; i < jarray.length(); i++) {
                subject = subject.concat(jarray.getString(i));
                subject = subject.concat(" ");
            }
            metaDataObject.setSubject(subject);
        } catch (Exception e) {}

        try {
            String publisher = (String) jobject.get("publisher");
            metaDataObject.setPublisher(publisher);
        } catch (Exception e) {}
        
        try {
            String type = (String) jobject.get("type");
            metaDataObject.setType(type);
        } catch (Exception e) {}

        try {
            String DOI = (String) jobject.get("DOI");
            metaDataObject.setDOI(DOI);
        } catch (Exception e) {}

        try {
            jarray = jobject.getJSONArray("ISSN");
            String issn = new String();
            for (int i = 0; i < jarray.length(); i++) {
                issn = issn.concat(jarray.getString(i));
                issn = issn.concat(" ");
            }
            metaDataObject.setISSN(issn);
        } catch (Exception e) {}

        try {
            jarray = jobject.getJSONArray("author");
            String author = new String();
            for (int i = 0; i < jarray.length(); i++) {
                JSONObject json = jarray.getJSONObject(i);
                String given = (String) json.get("given");
                author = author.concat(given);
                author = author.concat(" ");
                String family = (String) json.get("family");
                author = author.concat(family);
                author = author.concat(", ");
            }
            metaDataObject.setAuthor(author);
        } catch (Exception e) {}
    }

}
