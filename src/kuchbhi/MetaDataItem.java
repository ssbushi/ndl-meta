/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metaExtract;



public class MetaDataItem {

    private String author;

    private String creator;

    private String editor;

    private String dateCreated;

    private String dateAccessioned;//date the item was submitted to the repository

    private String dateCopyright;

    private String uri;//uniform resource identifier

    private String ISBN;

    private String DOI;

    private String ISSN;
    
    private String illustrator;

    private String citation;//bibliographic citation of the resource

    private String descriptionAbstract;

    private String tableOfContents;

    private String languageISO;//twoletter abbreviation for language

    private String subject;

    private String title;

    private String type;//nature/genre of file  text,journal,etc.

    private int numberOfPages;

    private String mimeType;//file format

    private String source;//the organisation from which the resource is harvested

    private String isPartOf;

    private String isPartOfSeries;

    private String hasPart;

    private String isReferencedBy;

    private String refersTo;

    private String relatedResource;

    private String rightsURI;

    private String rightsHolder;

    private String license;

    private String publisher;

    private String educationalLevel;

    private String typeOfLearningMaterial;/*types include exercise, simulation, questionnaire, diagram, figure, graph, slide,

     table, exam, experiment, problem statement, self assessment, lecture, book, tutorial*/


    private String typicalLearningTime;//expected time taken by reader to complete the learning object

    private choice difficultyLevel;

    private String board;//organisation affiliated institution under which use this in their curriculum

    private String prerequisiteTopic;//set of topics to be covered by learner to use this resource

    private String pedagogicObjective;//set of objectives a learner is to learn from this resource

    private String researcher;

    private String keyword;//List of keywords specified in thesis

    private String advisor;//A group of person supervising the thesis

    private String place;//The place of publication

    private String institution;

    private String awarded;//Ph.D. awarded date.

    private String degree;//degree awarded for this resource

    private String department;

    public MetaDataItem() {
        this.author = "";
        this.creator = "";
        this.editor = "";
        this.dateCreated = "";
        this.dateAccessioned = "";
        this.dateCopyright = "";
        this.uri = "";
        this.ISBN = "";
        this.DOI = "";
        this.ISSN = "";
        this.illustrator = "";
        this.citation = "";
        this.descriptionAbstract = "";
        this.tableOfContents = "";
        this.languageISO = "";
        this.subject = "";
        this.title = "";
        this.type = "";
        this.numberOfPages = 0;
        this.mimeType = "";
        this.source = "";
        this.isPartOf = "";
        this.isPartOfSeries = "";
        this.hasPart = "";
        this.isReferencedBy = "";
        this.refersTo = "";
        this.relatedResource = "";
        this.rightsURI = "";
        this.rightsHolder = "";
        this.license = "";
        this.publisher = "";
        this.educationalLevel = "";
        this.typeOfLearningMaterial = "";
        this.typicalLearningTime = "";
        this.difficultyLevel = null;
        this.board = "";
        this.prerequisiteTopic = "";
        this.pedagogicObjective = "";
        this.researcher = "";
        this.keyword = "";
        this.advisor = "";
        this.place = "";
        this.institution = "";
        this.awarded = "";
        this.degree = "";
        this.department = "";
    }

    
    public String getDOI() {

        return DOI;

    }

    public void setDOI(String DOI) {

        this.DOI = DOI;

    }

    public String getPublisher() {

        return publisher;

    }

    public void setPublisher(String publisher) {

        this.publisher = publisher;

    }

    public String getEducationalLevel() {

        return educationalLevel;

    }

    public void setEducationalLevel(String educationalLevel) {

        this.educationalLevel = educationalLevel;

    }

    public String getTypeOfLearningMaterial() {

        return typeOfLearningMaterial;

    }

    public void setTypeOfLearningMaterial(String typeOfLearningMaterial) {

        this.typeOfLearningMaterial = typeOfLearningMaterial;

    }

    public String getTypicalLearningTime() {

        return typicalLearningTime;

    }

    public void setTypicalLearningTime(String typicalLearningTime) {

        this.typicalLearningTime = typicalLearningTime;

    }

    public choice getDifficultyLevel() {

        return difficultyLevel;

    }

    public void setDifficultyLevel(choice difficultyLevel) {

        this.difficultyLevel = difficultyLevel;

    }

    public String getBoard() {

        return board;

    }

    public void setBoard(String board) {

        this.board = board;

    }

    public String getPrerequisiteTopic() {

        return prerequisiteTopic;

    }

    public void setPrerequisiteTopic(String prerequisiteTopic) {

        this.prerequisiteTopic = prerequisiteTopic;

    }

    public String getPedagogicObjective() {

        return pedagogicObjective;

    }

    public void setPedagogicObjective(String pedagogicObjective) {

        this.pedagogicObjective = pedagogicObjective;

    }

    public String getResearcher() {

        return researcher;

    }

    public void setResearcher(String researcher) {

        this.researcher = researcher;

    }

    public String getKeyword() {

        return keyword;

    }

    public void setKeyword(String keyword) {

        this.keyword = keyword;

    }

    public String getAdvisor() {

        return advisor;

    }

    public void setAdvisor(String advisor) {

        this.advisor = advisor;

    }

    public String getPlace() {

        return place;

    }

    public void setPlace(String place) {

        this.place = place;

    }

    public String getInstitution() {

        return institution;

    }

    public void setInstitution(String institution) {

        this.institution = institution;

    }

    public String getAwarded() {

        return awarded;

    }

    public void setAwarded(String awarded) {

        this.awarded = awarded;

    }

    public String getDegree() {

        return degree;

    }

    public void setDegree(String degree) {

        this.degree = degree;

    }

    public String getDepartment() {

        return department;

    }

    public void setDepartment(String department) {

        this.department = department;

    }

    public String getIsPartOf() {

        return isPartOf;

    }

    public void setIsPartOf(String isPartOf) {

        this.isPartOf = isPartOf;

    }

    public String getIsPartOfSeries() {

        return isPartOfSeries;

    }

    public void setIsPartOfSeries(String isPartOfSeries) {

        this.isPartOfSeries = isPartOfSeries;

    }

    public String getHasPart() {

        return hasPart;

    }

    public void setHasPart(String hasPart) {

        this.hasPart = hasPart;

    }

    public String getIsReferencedBy() {

        return isReferencedBy;

    }

    public void setIsReferencedBy(String isReferencedBy) {

        this.isReferencedBy = isReferencedBy;

    }

    public String getRefersTo() {

        return refersTo;

    }

    public void setRefersTo(String refersTo) {

        this.refersTo = refersTo;

    }

    public String getRelatedResource() {

        return relatedResource;

    }

    public void setRelatedResource(String relatedResource) {

        this.relatedResource = relatedResource;

    }

    public String getRightsURI() {

        return rightsURI;

    }

    public void setRightsURI(String rightsURI) {

        this.rightsURI = rightsURI;

    }

    public String getRightsHolder() {

        return rightsHolder;

    }

    public void setRightsHolder(String rightsHolder) {

        this.rightsHolder = rightsHolder;

    }

    public String getLicense() {

        return license;

    }

    public void setLicense(String license) {

        this.license = license;

    }

    public String getAuthor() {

        return author;

    }

    public void setAuthor(String author) {

        this.author = author;

    }

    public String getCreator() {

        return creator;

    }

    public void setCreator(String creator) {

        this.creator = creator;

    }

    public String getEditor() {

        return editor;

    }

    public void setEditor(String editor) {

        this.editor = editor;

    }

    public String getDateCreated() {

        return dateCreated;

    }

    public void setDateCreated(String dateCreated) {

        this.dateCreated = dateCreated;

    }

    public String getDateAccessioned() {

        return dateAccessioned;

    }

    public void setDateAccessioned(String dateAccessioned) {

        this.dateAccessioned = dateAccessioned;

    }

    public String getDateCopyright() {

        return dateCopyright;

    }

    public void setDateCopyright(String dateCopyright) {

        this.dateCopyright = dateCopyright;

    }

    public String getUri() {

        return uri;

    }

    public void setUri(String uri) {

        this.uri = uri;

    }

    public String getISBN() {

        return ISBN;

    }

    public void setISBN(String ISBN) {

        this.ISBN = ISBN;

    }

    public String getISSN() {

        return ISSN;

    }

    public void setISSN(String ISSN) {

        this.ISSN = ISSN;

    }

    public String getCitation() {

        return citation;

    }

    public void setCitation(String citation) {

        this.citation = citation;

    }

    public String getDescriptionAbstract() {

        return descriptionAbstract;

    }

    public void setDescriptionAbstract(String descriptionAbstract) {

        this.descriptionAbstract = descriptionAbstract;

    }

    public String getTableOfContents() {

        return tableOfContents;

    }

    public void setTableOfContents(String tableOfContents) {

        this.tableOfContents = tableOfContents;

    }

    public String getLanguageISO() {

        return languageISO;

    }

    public void setLanguageISO(String languageISO) {

        this.languageISO = languageISO;

    }

    public String getSubject() {

        return subject;

    }

    public void setSubject(String subject) {

        this.subject = subject;

    }

    public String getTitle() {

        return title;

    }

    public void setTitle(String title) {

        this.title = title;

    }

    public String getType() {

        return type;

    }

    public void setType(String type) {

        this.type = type;

    }

    public int getNumberOfPages() {

        return numberOfPages;

    }

    public void setNumberOfPages(int numberOfPages) {

        this.numberOfPages = numberOfPages;

    }

    public String getMimeType() {

        return mimeType;

    }

    public void setMimeType(String mimeType) {

        this.mimeType = mimeType;

    }

    public String getSource() {

        return source;

    }

    public void setSource(String source) {

        this.source = source;

    }

    public void printMetadata() {

        if (author != null && author.length() > 0) {

            System.out.println("Author: " + author);

        }

        if (creator != null && creator.length() > 0) {

            System.out.println("Creator: " + creator);

        }

        if (editor != null && editor.length() > 0) {

            System.out.println("Editor: " + editor);

        }

        if (publisher != null && publisher.length() > 0) {

            System.out.println("Publisher: " + publisher);

        }

        if (dateCreated != null && dateCreated.length() > 0) {

            System.out.println("Date Created: " + dateCreated);

        }

        if (dateAccessioned != null && dateAccessioned.length() > 0) {

            System.out.println("Date Accessioned : " + dateAccessioned);

        }

        if (dateCopyright != null && dateCopyright.length() > 0) {

            System.out.println("Date Copyright : " + dateCopyright);

        }

        if (uri != null && uri.length() > 0) {

            System.out.println("uri : " + uri);

        }

        if (ISBN != null && ISBN.length() > 0) {

            System.out.println("ISBN : " + ISBN);

        }

        if (ISSN != null && ISSN.length() > 0) {

            System.out.println("ISSN : " + ISSN);

        }

        if (DOI != null && DOI.length() > 0) {

            System.out.println("DOI : " + DOI);

        }

        if (citation != null && citation.length() > 0) {

            System.out.println("Citation : " + citation);

        }

        if (descriptionAbstract != null && descriptionAbstract.length() > 0) {

            System.out.println("Description Abstract : " + descriptionAbstract);

        }

        if (tableOfContents != null && tableOfContents.length() > 0) {

            System.out.println("Table of Contents : " + tableOfContents);

        }

        if (languageISO != null && languageISO.length() > 0) {

            System.out.println("Language ISO : " + languageISO);

        }

        if (subject != null && subject.length() > 0) {

            System.out.println("Subject : " + subject);

        }

        if (title != null && title.length() > 0) {

            System.out.println("Title : " + title);

        }

        if (type != null && type.length() > 0) {

            System.out.println("Type : " + type);

        }

        if (numberOfPages != 0) {

            System.out.println("Number Of Pages : " + numberOfPages);

        }

        if (mimeType != null && mimeType.length() > 0) {

            System.out.println("Mime Type : " + mimeType);

        }

        if (source != null && source.length() > 0) {

            System.out.println("Source : " + source);

        }

        if (rightsURI != null && rightsURI.length() > 0) {

            System.out.println("Rights URI : " + rightsURI);

        }

        if (rightsHolder != null && rightsHolder.length() > 0) {

            System.out.println("Rights Holder : " + rightsHolder);

        }

        if (license != null && license.length() > 0) {

            System.out.println("License : " + license);

        }

        if (isPartOf != null && isPartOf.length() > 0) {

            System.out.println("Is part of : " + isPartOf);

        }

        if (isPartOfSeries != null && isPartOfSeries.length() > 0) {

            System.out.println("Is part of series : " + isPartOfSeries);

        }

        if (hasPart != null && hasPart.length() > 0) {

            System.out.println("Has part : " + hasPart);

        }

        if (isReferencedBy != null && isReferencedBy.length() > 0) {

            System.out.println("Is referenced by : " + isReferencedBy);

        }

        if (refersTo != null && refersTo.length() > 0) {

            System.out.println("Refers to : " + refersTo);

        }

        if (relatedResource != null && relatedResource.length() > 0) {

            System.out.println("Related Resource : " + relatedResource);

        }

        if (educationalLevel != null && educationalLevel.length() > 0) {

            System.out.println("Educational Level : " + educationalLevel);

        }

        if (typeOfLearningMaterial != null && typeOfLearningMaterial.length() > 0) {

            System.out.println("Type Of Learning Material : " + typeOfLearningMaterial);

        }

        if (typicalLearningTime != null && typicalLearningTime.length() > 0) {

            System.out.println("Typical Learning Time : " + typicalLearningTime);

        }

        if (difficultyLevel != null) {

            System.out.println("Difficulty Level : " + difficultyLevel);

        }

        if (board != null && board.length() > 0) {

            System.out.println("Board : " + board);

        }

        if (prerequisiteTopic != null && prerequisiteTopic.length() > 0) {

            System.out.println("Prerequisite Topic : " + prerequisiteTopic);

        }

        if (pedagogicObjective != null && pedagogicObjective.length() > 0) {

            System.out.println("Pedagogic Objective : " + pedagogicObjective);

        }

        if (researcher != null && researcher.length() > 0) {

            System.out.println("Researcher  : " + researcher);

        }

        if (keyword != null && keyword.length() > 0) {

            System.out.println("Keyword  : " + keyword);

        }

        if (advisor != null && advisor.length() > 0) {

            System.out.println("Advisor  : " + advisor);

        }

        if (place != null && place.length() > 0) {

            System.out.println("Place  : " + place);

        }

        if (institution != null && institution.length() > 0) {

            System.out.println("Institution  : " + institution);

        }

        if (awarded != null && awarded.length() > 0) {

            System.out.println("Awarded  : " + awarded);

        }

        if (degree != null && degree.length() > 0) {

            System.out.println("Degree  : " + degree);

        }

        if (department != null && department.length() > 0) {

            System.out.println("Department  : " + department);

        }
        if (illustrator != null && illustrator.length() > 0) {

            System.out.println("Illustrator  : " + illustrator);

        }

    }

    /**
     * @return the illustrator
     */
    public String getIllustrator() {
        return illustrator;
    }

    /**
     * @param illustrator the illustrator to set
     */
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

}
