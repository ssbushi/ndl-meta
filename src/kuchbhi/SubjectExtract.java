/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metaExtract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;
import static metaExtract.MainPage.metaDataObject;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author biswarupb
 */
public class SubjectExtract {
    
    public static boolean SubjectGet(String text)
    {
        try 
        {
            String lang = "";
            System.out.println("yesyesyes");
            if(metaDataObject.getLanguageISO().toLowerCase().contains("en"))
                lang = "en";
            else if(metaDataObject.getLanguageISO().toLowerCase().contains("de"))
                lang = "de";
            else
                lang = "auto";
            String url = "http://clfapi.base-search.net/classify?text=" + text + "&language=" + lang + "&format=json";
            //System.out.println(url);
            url = url.replaceAll(" ", "%20");
            JSONObject json1 = new JSONObject(getUrlSource(url));
            String subject = "";
            String number = "";
            if(json1.has("1"))
            {
                JSONArray json2 = json1.getJSONArray("1");
                JSONObject json3 = json2.getJSONObject(0);
                subject = (String) json3.get("heading");
                number = (String) json3.get("number");
            }
            if(json1.has("2"))
            {
                JSONArray json2 = json1.getJSONArray("2");
                JSONObject json3 = json2.getJSONObject(0);
                subject += ", " + (String) json3.get("heading");
                number = (String) json3.get("number");
            }
            if(json1.has("3"))
            {
                JSONArray json2 = json1.getJSONArray("3");
                JSONObject json3 = json2.getJSONObject(0);
                subject += ", " + (String) json3.get("heading");
                number = (String) json3.get("number");
            }
            System.out.println(subject);
            System.out.println(number);
            if(metaDataObject.getSubject() == null || "".equals(metaDataObject.getSubject()))
                        metaDataObject.setSubject(subject);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static String getUrlSource(String url) throws IOException
    {
        URL yahoo = new URL(url);
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.3.100.207", 8080));
        HttpURLConnection yc = (HttpURLConnection) yahoo.openConnection(proxy);
//        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
        BufferedReader reader = new BufferedReader(new InputStreamReader(((HttpURLConnection) (new URL(url)).openConnection()).getInputStream(), Charset.forName("UTF-8")));
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = reader.readLine()) != null)
            a.append(inputLine);
        reader.close();
        return a.toString();
    }
    
}
