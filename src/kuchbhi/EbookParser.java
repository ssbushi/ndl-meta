/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metaExtract;

/**
 *
 * @author biswarupb
 */

import java.io.*;
import java.util.Enumeration;
import java.util.zip.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import static metaExtract.MainPage.corePath;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import static metaExtract.MainPage.metaDataObject;

public class EbookParser {
//    static String path = "/home/biswarupb/NetBeansProjects/ndl-meta/src/Documents/";
//    
//    public static void main(String[] args) {
//        getZipFiles("/home/biswarupb/NetBeansProjects/ndl-meta/src/Documents/The Alchemist - Coelho_ Paulo.epub");
//    }
 
    public static void getZipFiles(String filename)
    {
        try
        {
            ZipFile zipFile = new ZipFile(filename);

            Enumeration files = zipFile.entries();

            while (files.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) files.nextElement();
                if (entry.isDirectory()) {
                    File file = new File(corePath + "/" + entry.getName());
                    file.mkdir();
                    //System.out.println("Create dir " + entry.getName());
                    if(entry.getName().substring(entry.getName().length()-3).equalsIgnoreCase("opf"))
                    {
                        XMLParse(entry.getName());
                        ProcessBuilder Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/" + entry.getName());
                        Process p = Process.start();
                        p.waitFor();
                    }
                    else
                    {
                        ProcessBuilder Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/" + entry.getName());
                        Process p = Process.start();
                        p.waitFor();
                    }
                } else {
                    File f = new File(corePath + "/" + entry.getName());
                    FileOutputStream fos = new FileOutputStream(f);
                    InputStream is = zipFile.getInputStream(entry);
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        fos.write(buffer, 0, bytesRead);
                    }
                    fos.close();
                    //System.out.println("Create File " + entry.getName());
                    if(entry.getName().substring(entry.getName().length()-3).equalsIgnoreCase("opf"))
                    {
                        XMLParse(entry.getName());
                        ProcessBuilder Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/" + entry.getName());
                        Process p = Process.start();
                        p.waitFor();
                        break;
                    }
                    else
                    {
                        ProcessBuilder Process = new ProcessBuilder("sh", "-c", "rm " + corePath + "/" + entry.getName());
                        Process p = Process.start();
                        p.waitFor();
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public static void XMLParse(String contentPath)
    {
        try {
 
            File fXmlFile = new File(corePath + "/" + contentPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            ////System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            Element eElement1 = (Element) doc.getElementsByTagName("package").item(0);
            Element eElement = (Element) eElement1.getElementsByTagName("metadata").item(0);
            
            try
            {
                NodeList nList = eElement.getElementsByTagName("meta");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        if((e.getAttribute("name")).equalsIgnoreCase("calibre:timestamp"))
                        {
                            //System.out.println(e.getAttribute("content"));
                            metaDataObject.setDateCreated(e.getAttribute("content"));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                String subject = "";
                NodeList nList = eElement.getElementsByTagName("dc:subject");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        //System.out.println(e.getTextContent());
                        if(i!=0)
                            subject += "; " + e.getTextContent();
                        else
                            subject = e.getTextContent();
                    }
                }
                metaDataObject.setSubject(subject);
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                NodeList nList = eElement.getElementsByTagName("dc:date");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        try
                        {
                            if((e.getAttribute("opf:event")).equalsIgnoreCase("publication"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setDateCopyright(e.getTextContent());
                            }
                            if((e.getAttribute("opf:event")).equalsIgnoreCase("conversion"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setDateCopyright(e.getTextContent());
                            }
                            if((e.getAttribute("opf:event")).equalsIgnoreCase("creation"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setDateCreated(e.getTextContent());
                            }
                            if((e.getAttribute("opf:event")).equalsIgnoreCase("modification"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setDateAccessioned(e.getTextContent());
                            }
                        }
                        catch(Exception exx)
                        {
                            exx.printStackTrace();
                            //System.out.println(e.getTextContent());
                            metaDataObject.setDateCreated(e.getTextContent());
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                NodeList nList = eElement.getElementsByTagName("dc:identifier");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        try
                        {
                            if((e.getAttribute("opf:scheme")).equalsIgnoreCase("ISBN"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setISBN(e.getTextContent());
                            }
                            if((e.getAttribute("opf:scheme")).equalsIgnoreCase("ISSN"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setISSN(e.getTextContent());
                            }
                            if((e.getAttribute("opf:scheme")).equalsIgnoreCase("DOI"))
                            {
                                //System.out.println(e.getTextContent());
                                metaDataObject.setDOI(e.getTextContent());
                            }
                        }
                        catch(Exception exx)
                        {
                            exx.printStackTrace();
                            //System.out.println(e.getTextContent());
                            metaDataObject.setUri(e.getTextContent());
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:title").item(0).getTextContent());
                metaDataObject.setTitle(eElement.getElementsByTagName("dc:title").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:description").item(0).getTextContent());
                metaDataObject.setDescriptionAbstract(eElement.getElementsByTagName("dc:description").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:type").item(0).getTextContent());
                metaDataObject.setType(eElement.getElementsByTagName("dc:type").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:format").item(0).getTextContent());
                metaDataObject.setMimeType(eElement.getElementsByTagName("dc:format").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:source").item(0).getTextContent());
                metaDataObject.setSource(eElement.getElementsByTagName("dc:source").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:language").item(0).getTextContent());
                metaDataObject.setLanguageISO(eElement.getElementsByTagName("dc:language").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:relation").item(0).getTextContent());
                metaDataObject.setRelatedResource(eElement.getElementsByTagName("dc:relation").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:coverage").item(0).getTextContent());
                metaDataObject.setLicense(eElement.getElementsByTagName("dc:coverage").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

            try
            {
                //System.out.println(eElement.getElementsByTagName("dc:rights").item(0).getTextContent());
                metaDataObject.setRightsHolder(eElement.getElementsByTagName("dc:rights").item(0).getTextContent());
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                NodeList nList = eElement.getElementsByTagName("dc:creator");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        try
                        {
                            if((e.getAttribute("opf:role")).equalsIgnoreCase("aut"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aqt"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aft"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aui"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("bkp"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getPublisher() == null || "".equals(metaDataObject.getPublisher()))
                                    metaDataObject.setPublisher(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ill"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("dsr"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("pht"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("art"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("edt"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getEditor() != null || !("".equals(metaDataObject.getEditor())))
                                    metaDataObject.setEditor(metaDataObject.getEditor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setEditor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ths"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAdvisor() != null || !("".equals(metaDataObject.getAdvisor())))
                                    metaDataObject.setAdvisor(metaDataObject.getAdvisor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAdvisor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ant"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getCitation() != null || !("".equals(metaDataObject.getCitation())))
                                    metaDataObject.setCitation(metaDataObject.getCitation() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setCitation(e.getTextContent());
                            }
                        }
                        catch(Exception exx)
                        {
                            exx.printStackTrace();
                            //System.out.println(e.getTextContent());
                            if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                NodeList nList = eElement.getElementsByTagName("dc:contributor");
                for (int i = 0; i < nList.getLength(); i++) {
                    if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                        Element e = (Element) nList.item(i);                    
                        try
                        {
                            if((e.getAttribute("opf:role")).equalsIgnoreCase("aut"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aqt"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aft"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("aui"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("bkp"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getPublisher() == null || "".equals(metaDataObject.getPublisher()))
                                    metaDataObject.setPublisher(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ill"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("dsr"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("pht"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("art"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getIllustrator() != null || !("".equals(metaDataObject.getIllustrator())))
                                    metaDataObject.setIllustrator(metaDataObject.getIllustrator() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setIllustrator(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("edt"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getEditor() != null || !("".equals(metaDataObject.getEditor())))
                                    metaDataObject.setEditor(metaDataObject.getEditor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setEditor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ths"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getAdvisor() != null || !("".equals(metaDataObject.getAdvisor())))
                                    metaDataObject.setAdvisor(metaDataObject.getAdvisor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAdvisor(e.getTextContent());
                            }
                            else if((e.getAttribute("opf:role")).equalsIgnoreCase("ant"))
                            {
                                //System.out.println(e.getTextContent());
                                if(metaDataObject.getCitation() != null || !("".equals(metaDataObject.getCitation())))
                                    metaDataObject.setCitation(metaDataObject.getCitation() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setCitation(e.getTextContent());
                            }
                        }
                        catch(Exception exx)
                        {
                            exx.printStackTrace();
                            //System.out.println(e.getTextContent());
                            if(metaDataObject.getAuthor() != null || !("".equals(metaDataObject.getAuthor())))
                                    metaDataObject.setAuthor(metaDataObject.getAuthor() + "; " + e.getTextContent());
                                else
                                    metaDataObject.setAuthor(e.getTextContent());
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
            try
            {
                String newline = System.getProperty("line.separator");
                String toc = "";
                Element eElement2 = (Element) eElement1.getElementsByTagName("spine").item(0);
                if(eElement.getAttribute("toc") != null)
                {
                    NodeList nList = eElement2.getElementsByTagName("itemref");
                    for (int i = 0; i < nList.getLength(); i++) {
                        if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {             
                            Element e = (Element) nList.item(i);
                            //System.out.println(e.getAttribute("idref"));
                            toc += (i+1) + ". " + e.getAttribute("idref") + newline;
                        }
                    }
                    metaDataObject.setTableOfContents(toc);
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("EPUB Processed!");
    }
}