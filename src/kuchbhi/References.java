package metaExtract;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class References {

    public static String getReferences(String file) {
        System.out.println("in references");
        StringBuilder ref=null;
        try {
            ProcessBuilder pbuilder = new ProcessBuilder("pdf-extract", "extract", "--references", file);
            Process p = pbuilder.start();
            BufferedReader reader;
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sbuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sbuilder.append(line);
                sbuilder.append(System.getProperty("line.separator"));
            }
            String result = sbuilder.toString();
            p.waitFor();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(result));
            Document doc = builder.parse(is);
                //Getting the writers ready
            //Root element - outlines
            Element root = doc.getDocumentElement();
            NodeList nList = root.getElementsByTagName("reference");
            ref = new StringBuilder();
            //loop over each outline element
            for (int i = 0; i < nList.getLength(); i++) {
                String temp = nList.item(i).getTextContent();
                //System.out.println((i + 1) + "---" + temp + "\n");
                ref.append(i + 1).append("---").append(temp).append(System.getProperty("line.separator"));
                //}
            }
            //closing resources
        } catch (IOException | InterruptedException | ParserConfigurationException | SAXException | DOMException ex) {
            try {
                System.out.println("exception thrown in references");                // Logger.getLogger(getReferences.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex1) {
                //Logger.getLogger(getReferences.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        if(ref != null)
            return ref.toString();
        else return null;
    }
}
