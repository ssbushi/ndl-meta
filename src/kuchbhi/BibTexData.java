package metaExtract;

import java.util.ArrayList;
import java.util.Iterator;

public class BibTexData
{
    ArrayList<BibTexPair> pairs = new ArrayList<>();
    String type;
    String code;
    public void addPair(String field,String val)
    {
        pairs.add(new BibTexPair(field, val));
    }
    public void removePair(String field)
    {
        Iterator itr = pairs.iterator();
        while(itr.hasNext())
        {
            BibTexPair bp = (BibTexPair)itr.next();
            if(bp.getField().equalsIgnoreCase(field.trim()))
            {
                itr.remove();
                break;
            }
        }
    }
    public void printData()
    {
        System.out.println("@"+type+":"+code);
        System.out.println("{");
        pairs.stream().forEach((p) -> {
            System.out.println(p.getField()+"={"+p.getValue()+"}");
        });
        System.out.println("}");
    }
}
/**
 *
 * @author ssbushi
 */
//Class to store a BibTex Pair like title:Harry Potter, etc
class BibTexPair 
{
    private final String field;

    public String getField() {
        return field;
    }
    public String getValue() {
        return value;
    }
    private final String value;
    public BibTexPair(String field, String value) 
    {
        this.field = field;
        this.value = value;
    }    
}