package metaExtract;

import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

public class getTitleforPDF {

//    public static void main(String[] args) throws Exception {
//        String replaceAll = "bfm%3A978-3-319-16048-1%2F1%2F1.pdf".replaceAll("[^a-zA-Z0-9\\._+-]", "\\\\$0");
//        System.out.println(replaceAll);
////        System.out.println("Out-Title: " + getTitle("/home/ndl-10/Downloads/art%3A10.1007%2Fs12239-015-0085-3.pdf"));
////        System.out.println("Out-Title: " + getTitle("/home/ndl-10/Downloads/" + replaceAll));
//        System.out.println("Out-Title: " + getTitle("/home/ndl-10/Downloads/b5d evelopment.pdf" ));
////        System.out.println("Out-Title: " + getTitle("/home/ndl-10/Downloads/FJPS_36_1_2009.pdf" ));
//    }

    public static String getTitle(String file) throws Exception {
        BufferedReader reader = null;
        String line;
        try {
            ProcessBuilder pbuilder = new ProcessBuilder("pdf-extract", "extract", "--titles", file);
            Process p = pbuilder.start();
            
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sbuilder = new StringBuilder();

            while ((line = reader.readLine()) != null) {
//                 System.out.println("here: "+line);
                sbuilder.append(line);
                sbuilder.append(System.getProperty("line.separator"));
            }

            String result = sbuilder.toString();
            System.out.println("here: " + result);
            p.waitFor();
            //we generate output as outXML as shown. This is parsed.
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(result));
            Document doc = builder.parse(is);
            //Root element - outlines
            Element root = doc.getDocumentElement();
            System.out.println("Title :"+root.getElementsByTagName("title").item(0).getTextContent());
            return root.getElementsByTagName("title").item(0).getTextContent();

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
