package metaExtract;

import java.io.*;
import java.net.*;

import java.nio.charset.*;
import java.util.regex.*;
import static metaExtract.MainPage.metaDataObject;
import org.json.JSONObject;

/**
 * Extracts the ISBN, get the JSON details from Google Books API and finally
 * does some pretty printing.
 */
public class findISBNJSON {

    public static JSONObject findISBN() throws Exception 
    {
        //Get the ISBN
        
        String ISBN = searchISBN();
        ISBN = cleanISBN(ISBN);
        metaDataObject.setISBN(ISBN);
        System.out.println("ISBN:" + ISBN);
        JSONObject json = readJsonFromUrl("https://www.googleapis.com/books/v1/volumes?q=isbn:" + ISBN);
        //First attempt
        if ((json.get("totalItems")).toString().equals("0")) //If no result, then try this
        {

//            /*Somework to be done here */
//            json = readJsonFromUrl("https://www.googleapis.com/books/v1/volumes?q=isbn" + ISBN);
//            //Second attempt
//            if ((json.get("totalItems")).equals("0")) //If still no result is found (not quite likely)
//            {
//                //do some processing
//                System.out.println("No suitable result found using Google Books API!");
                return null;
//            } else {
//                return json;
//            }

        } else {
            return json;
        }
    }

    private static String searchISBN() throws Exception {
//        int lines = (int) (metaDataObject.getNumberOfPages() * 10);
//        int count = 0;
        String isbn;
//        String[] textLines=MainPage.textFile.split("\\n");

//        for (String line: textLines) {
//            count++;
//            if (count > lines) {
//                break;
//            }
            String pattern1 = "ISBN(-1(?:(0)|3))?:?\\x20(\\s)*[0-9]+[- ][0-9]+[- ][0-9]+[- ][0-9]*[- ]*[xX0-9]";
            String pattern2 = "ISSN(\\s)*[:]{0,1}(\\s)*[0-9][0-9][0-9][0-9][-][0-9][0-9][0-9][X0-9]";
            Pattern r = Pattern.compile(pattern2);
            Matcher m = r.matcher(MainPage.textFile);
            if (m.find()) {
                String issn = m.group();
                issn = issn.replaceAll("ISSN", "");
                issn = issn.replaceAll("issn", "");
                metaDataObject.setISSN(issn);
            }
            r = Pattern.compile(pattern1);
            m = r.matcher(MainPage.textFile);
            if (m.find()) {
                isbn = m.group();
                return isbn;
            }
//        }
        throw new Exception();
    }

    private static String cleanISBN(String ISBN) {
        int length = 0;
        String ISBNclean = "";
        String ISBNcleaned = "";
        for (int i = ISBN.length() - 1; i >= 0; i--) {
            if (length == 13) {
                break;
            }
            if (ISBN.charAt(i) >= '0' && ISBN.charAt(i) <= '9') {
                if (length < 10) {
                    ISBNclean += ISBN.charAt(i);
                    length++;
                } else {
                    if (ISBN.charAt(i) == '9' || ISBN.charAt(i) == '8' || ISBN.charAt(i) == '7') {
                        ISBNclean += ISBN.charAt(i);
                        length++;
                    }
                }

            }
        }

        for (int i = ISBNclean.length() - 1; i >= 0; i--) {
            ISBNcleaned += ISBNclean.charAt(i);
        }
        return ISBNcleaned;
    }

    public static String readAll(Reader rd) throws Exception//read the JSON from the URL
    {

        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();

    }

    public static JSONObject readJsonFromUrl(String url) //read JSON from URL
    {
        InputStream is = null;
        BufferedReader rd = null;
        try {
            is = new URL(url).openStream();
            rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));     //Unicode format chosen
            String jsonText = readAll(rd);
            return new JSONObject(jsonText);                                                                                     //returns the JSON object for checking in the calling func.
        } catch (Exception e) {}
        try{
            if( is!= null )
                is.close();
            if( rd != null)
                rd.close();
        } catch (Exception e){}
        
        return null;
    }

}
