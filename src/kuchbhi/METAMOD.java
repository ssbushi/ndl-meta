package metaExtract;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;


/**
 * Code to fetch Bibtex from Google Scholar,
 * using text extractiong and cookies.
 * Returns an ArrayList of BibTexData objects
 * They MUST be filtered and cross checked to be of any use.
 * Use with fetchLines function , giving absolute filepath(.txt)
 */
public class METAMOD {

    private static final String baseURL = "http://scholar.google.com/scholar?q=";

    private static ArrayList<String> fetchLines(String fileContent) {
        ArrayList<String> ret = null;
        try {            
            int nlines = 0;
            ArrayList<String> lines = new ArrayList<>();
            ArrayList<String> order = new ArrayList<>();
            String[] texlines = fileContent.split("\\n");
            for (String line:texlines) {
                if (line.trim().length() > 0) {
                    nlines++;
                    lines.add(line);
                    order.add(line);
                }
            }
            Iterator<String> it = lines.iterator();
            int count = 0;
            while (it.hasNext()) {
                count++;
                String s = it.next();
                if (count < nlines / 2 || s.trim().length() < 30) {
                    it.remove();
                }
            }
            it = order.iterator();
            count = 0;
            while (it.hasNext()) {
                count++;
                it.next();
                if (count < nlines / 2) {
                    it.remove();
                }
            }
            ArrayList<Pair> chunks = new ArrayList<>();
            Collections.sort(lines, (String p1, String p2) -> p1.length() - p2.length());
            nlines = lines.get(lines.size() / 2).length();
            int start = 0;
            int end;
            for (int i = 0; i < order.size(); i++) {
                String s = order.get(i);
                if (s.length() >= 0.9 * nlines && s.length() <= 1.1 * nlines) {
                    if (start == 0) {
                        start = i;
                    }
                } else {
                    if (start > 0) {
                        end = i - 1;
                        chunks.add(new Pair(start, end));
                        start = 0;
                    }
                }
            }
            Collections.sort(chunks, (Pair p1, Pair p2) -> ((int) p1.getKey() - (int) p1.getValue()) - ((int) p2.getKey() - (int) p2.getValue()));
            ret = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                Pair x = chunks.get(i);
                String s = "";
                for (int j = (int) x.getKey(); j <= (int) x.getValue(); j++) {
                    s += " " + order.get(j);
                }
//                System.out.println("This chunk: " + s);
                String[] words = s.split("[\\s]+");
                for (int j = 0; j < words.length; j++) { //[^a-zA-Z\\,\\:\\;\\(\\)\\.\\-]
                    if (words[j].matches("[^a-zA-Z\\,\\:\\;\\(\\)\\.\\-]")) {
                        words[j] = null;
                    }
                }
                int sdex = 0;
                int edex = 0;
                int max = 0;
                int smax = 0;
                for (int j = 0; j < words.length;) {
//                    System.out.println("loop " + j);
                    if (words[j] != null) {
                        sdex = j;
                        edex = j + 1;
                        for (int k = j + 1; k < words.length; k++) {
                            if (words[k] == null) {
                                break;
                            } else {
                                edex = k;
                            }
                        }
                        if (max < edex - sdex) {
                            max = edex - sdex;
                            smax = sdex;
                        }
                        j = edex + 1;
                    } else {
                        j++;
                    }
                }
                s = "";
                for (int j = smax; j <= smax + max; j++) {
                    s += words[j].trim() + " ";
                }
                words = s.split("[\\s]+");
                if (words.length < 16) {
                    ret.add(s);
                    System.out.println("STR: " + s);
                } else {
                    String temp = "";
                    int mid = words.length / 2;
                    for (int j = mid - 8; j < mid + 8; j++) {
                        temp += words[j].trim() + " ";
                    }
                    ret.add(temp);
                    System.out.println("STR: " + temp);
                }
            }
        } catch (Exception e) {
        }
        return ret;
    }

    private static ArrayList<BibTexData> getBibtex(ArrayList<String> in) {
        System.setProperty("http.proxyHost", "10.3.100.207");
        System.setProperty("http.proxyPort", "8080");
        System.setProperty("https.proxyHost", "10.3.100.207");
        System.setProperty("https.proxyPort", "8080");
        Set<String> tex = new HashSet<>();
        GetMethod method = null;
        try {
            //Getting cookie to store
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpGet request = new HttpGet("http://scholar.google.com");
            request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5");
            HttpHost proxy = new HttpHost("10.3.100.207", 8080);
            httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
            HttpResponse response = httpclient.execute(request);
            CookieStore cookieStore = httpclient.getCookieStore();
            List<Cookie> cookies = cookieStore.getCookies();
            CookieStore newStore = new BasicCookieStore();
            cookies.stream().map((c) -> {
                String val = c.getValue();
                if (c.getName().trim().equals("GSP")) {
                    val += ":CF=4";
                }
                BasicClientCookie cookie = new BasicClientCookie(c.getName(), val);
                cookie.setDomain(c.getDomain());
                return cookie;
            }).map((cookie) -> {
                cookie.setPath("/");
                return cookie;
            }).forEach((cookie) -> {
                newStore.addCookie(cookie);
            });
            httpclient.setCookieStore(newStore);
            for (String s : in) {
                String test = "\"" + s.trim() + "\"";
                test = URLEncoder.encode(test, "UTF-8");
                test += "&hl=en";
                String url = baseURL + test;
                System.out.println("URL:" + url);
                HttpGet httpget = new HttpGet(url);
                EntityUtils.consume(response.getEntity());
                response = httpclient.execute(httpget);
                String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
                Pattern p = Pattern.compile("q=info:[^:]+");
                Matcher matcher = p.matcher(responseString);
                Set<String> set = new HashSet<>();
                while (matcher.find()) {
                    String fit = matcher.group();
                    set.add(fit);
                }
                EntityUtils.consume(response.getEntity());
                for (String id : set) {
                    httpget = new HttpGet("http://scholar.google.com/scholar.bib?" + id + ":scholar.google.com/&output=citation");
                    Thread.sleep(1000);
                    response = httpclient.execute(httpget);
                    responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
//                    System.out.println(responseString);
                    tex.add(responseString);
                }
            }            

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(METAMOD.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(METAMOD.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        return parseResponse(tex);
    }

    private static ArrayList<BibTexData> parseResponse(Set<String> tex) {
        BibTexData bt = new BibTexData();
        ArrayList<BibTexData> allResults = new ArrayList<>();
        try{
            for (String texdoc : tex) {
                String lines[] = texdoc.split("\\r?\\n");
                for (String line : lines) {
                    line = line.trim();
                    if (line.startsWith("@")) {
                        String[] parts = line.split("\\{");
                        if (parts[1].length() > 1) {
                            parts[1] = parts[1].substring(0, parts[1].length() - 1);
                        }
                        bt.type = parts[0];
                        bt.code = parts[1];
                    } else if (line.equals("}")) {
                        allResults.add(bt);
                        bt = new BibTexData();
                    } else {
                        String[] parts = line.split("=");
                        if (!(parts[1].equals("{},") || parts[1].equals("{}"))) {
                            if (parts[1].endsWith("},")) {
                                parts[1] = parts[1].substring(1, parts[1].length() - 2);
                            }
                            if (parts[1].endsWith("}") && parts[1].length() > 2) {
                                parts[1] = parts[1].substring(1, parts[1].length() - 1);
                            }
                            bt.addPair(parts[0], parts[1]);
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return allResults;
    }
    //the only exposed method
    public static ArrayList<BibTexData> fetchBibData(String textfile) {
        ArrayList<String> stuff;
        stuff = fetchLines(textfile);
        ArrayList<BibTexData> res = getBibtex(stuff);
        return res;
    }
}
