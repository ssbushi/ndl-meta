package metaExtract;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.Math.min;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import static metaExtract.MainPage.filePath;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;


public class Wikifier {

    static String baseUrl = "http://wikipedia-miner.cms.waikato.ac.nz/services/wikify?source=";

    private static ArrayList<String> wikifyText(String source) throws SAXException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
//        System.out.println(baseUrl + source);
        Document doc = db.parse(new URL(baseUrl + source).openStream());
//        DOMSource domSource = new DOMSource(doc);
//        StringWriter writer = new StringWriter();
//        StreamResult result = new StreamResult(writer);
//        TransformerFactory tf = TransformerFactory.newInstance();
//        Transformer transformer = tf.newTransformer();
//        transformer.transform(domSource, result);
//        System.out.println("XML IN String format is: \n" + writer.toString());
        Element root = doc.getDocumentElement();
        Element tpx = (Element) root.getElementsByTagName("detectedTopics").item(0);        
//        System.out.println("TPX: "+tpx.getNodeName());
        NodeList nList = tpx.getElementsByTagName("detectedTopic");
        ArrayList<String> topics = new ArrayList<>();
        //loop over each outline element
        for (int i = 0; i < nList.getLength(); i++) {
            if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                //fetch necessary data in pretty format                
                Element e = (Element) nList.item(i);                    
                topics.add(e.getAttribute("title"));
            }
        }
        return topics;
    }

    private static  ArrayList<String> keyWords = new ArrayList();
    private static final int max_size = 1500;
    private static String textFile;

    private static void callMe() {
        int start = textFile.length() / 10;
        int end = textFile.length() / 5;
        ArrayList<String> temp;
        for (int i = 0; i <5; i++) {

            try {
                String query = textFile.substring(start, end);
                String encodedURL = java.net.URLEncoder.encode(query.substring(0, min(max_size, query.length())), "UTF-8");
                temp = wikifyText(encodedURL);
                for (String temp1 : temp) {
                    if (!keyWords.contains(temp1)) {
                        keyWords.add(temp1);
                    }
                }
                start = (2 *i+3) * (textFile.length() / 10);
                end = (i+2) * textFile.length() / 5;
            } catch (SAXException | IOException | ParserConfigurationException | TransformerException ex) {
                Logger.getLogger(Wikifier.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static String findKeyWords() throws Exception {
        
        
        Metadata metadata = new Metadata();
        PrintWriter writer = null;
        String writeContent;
        InputStream input = null;
        
        try {
                input = new FileInputStream(new File(filePath));
                ContentHandler textHandler = new BodyContentHandler();
                Parser parser = new AutoDetectParser();
                ParseContext context = new ParseContext();
                try {
                    parser.parse(input, textHandler, metadata, context);
                } catch (Exception e) {
                    //It is there for convenience
                }
                writeContent = textHandler.toString();
                writeContent = writeContent.substring(0, Math.min(writeContent.length(), 99998));
                textFile=writeContent;
        }catch(Exception e){
            textFile = MainPage.textFile;
        }
        finally{
            if(textFile.length() == 0 || textFile == null){
                textFile = MainPage.textFile;
            }
        }
        
        
        String keyreturn = "";
        callMe();
        int count[][] = new int[keyWords.size()][2];
        for (int i = 0; i < keyWords.size(); i++) {
            int j = 0;
            String key = keyWords.get(i).toLowerCase();
            Pattern p = Pattern.compile(" "+key);
            Matcher m = p.matcher(textFile.toLowerCase());
            
            while (m.find()) {
                j++;
            }
            count[i][0] = j;
            count[i][1] = i;
        }
        System.out.println("Size :"+count.length);
        for (int i = 0; i < count.length; i++) {
            int pos = i;
            int temp;
            for (int j = i + 1; j < count.length; j++) {
                if (count[j][0] > count[pos][0]) {
                    pos = j;
                }
            }
            if (pos != i) {
                temp = count[pos][0];
                count[pos][0] = count[i][0];
                count[i][0] = temp;
                temp = count[pos][1];
                count[pos][1] = count[i][1];
                count[i][1] = temp;
            }
        }
//        for (int i = 0; i < count.length; i++) {
//            System.out.println("count of " +keyWords.get(count[i][1])+"is"+count[i][0]);
//            
//        }
        for (int i = 0; i < min(count.length, 7); i++) {

            keyreturn += keyWords.get(count[i][1]) + ", ";
        }
        keyreturn = keyreturn.substring(0,keyreturn.length()-2);
        System.out.println("Keywords : " + keyreturn);
        return keyreturn;
    }
}
