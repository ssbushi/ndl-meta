package metaExtract;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import static metaExtract.MainPage.filePath;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Extracts TOC from pdf and outputs file as fileName.toc INPUT - absolute path
 * to pdf file 
 * RETURNS - String
 * OUTPUT - void *
 */
public class TOCBuilder {

    public static String getTOC() {
        StringBuilder toc=null;
        BufferedReader reader;
        String line;
        
        try {
            ProcessBuilder pbuilder = new ProcessBuilder("bash", "-c", "dumppdf.py -T " + filePath);
            Process p = pbuilder.start();
            
            reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sbuilder = new StringBuilder();
          
            while ((line = reader.readLine()) != null) {
                sbuilder.append(line);
                sbuilder.append(System.getProperty("line.separator"));
            }
            String result = sbuilder.toString();
            p.waitFor();
            //we generate output as outXML as shown. This is parsed.
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(result));
            Document doc = builder.parse(is);
            //Root element - outlines
            Element root = doc.getDocumentElement();
            NodeList nList = root.getElementsByTagName("outline");
            //loop over each outline element
            toc=new StringBuilder();
            for (int i = 0; i < nList.getLength(); i++) {
                if (nList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    //fetch necessary data in pretty format
                    String temp = "";
                    Element e = (Element) nList.item(i);
                    for (int j = 1; j < Integer.parseInt(e.getAttribute("level")); j++) {
                        temp += "\t";
                    }
                    temp += e.getAttribute("title") + "----------------";
                    temp += e.getElementsByTagName("pageno").item(0).getTextContent();
                    toc.append(temp);
                    toc.append(System.getProperty("line.separator"));
                }
            }
        } catch (IOException | InterruptedException | ParserConfigurationException | SAXException | NumberFormatException | DOMException ex) {
            
        }
        if( toc!=null)
            return toc.toString();
        return null;
    }
//    public static void main(String[] args) {
//        try {
//            filePathTerminal="/home/ndl-4/sd/cormen.pdf";
//            System.out.println(getTOC());
//        } catch (Exception ex) {
//            Logger.getLogger(getTitleforPDF.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
}
