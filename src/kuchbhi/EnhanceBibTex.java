/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metaExtract;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Fetch more metadata using the existing identification
 * of bibtex
 * using google books api
 * INPUT: BibTexData Object and tolerance(default:0.8)
 * OUTPUT: Null
 * RETURNS: JSON object if found, else null
 * @author ssbushi
 */
public class EnhanceBibTex 
{
    private static final String url = "https://www.googleapis.com/books/v1/volumes?q=";
    //private static String key="&key=AIzaSyCLENr2mBCU6SFENizJp__-W5iiswP9PUs";
    private static JSONObject retObj = null;
    public static JSONObject fetchData(BibTexData data) throws IOException
    {
        return fetchData(data, 0.8);
    }
    //main method exposed to user. fetches json object if match is found. else null
    public static JSONObject fetchData(BibTexData data,double tolerance)
    {
        String term1="";
        String term2="";
        String words[] = null;
        String names[] = null;
        String title = null;
        for(BibTexPair p: data.pairs)
        {            
            if(p.getField().toLowerCase().contains("title"))
            {
                //title term
                title=p.getValue().trim();
                term1=getTerm("intitle","\""+p.getValue().trim()+"\"");
            }
            if(p.getField().toLowerCase().contains("author"))
            {
                //get names from BIbtex
                String temp = p.getValue();
                names = temp.split(" and ");
                for(int i=0;i<names.length;i++)
                {
                    names[i]=names[i].replaceAll(",","");
                }
            }
        }
        for(String name: names)
        {
            words = name.split("[\\s]+");
            for(String i:words)
            {
                if(i.length()>1)
                {
                    //check by author all possible names. if one satisfying match is found. Stop
                    term2=getTerm("inauthor",i);
                    System.out.println(url+term1+"+"+term2);
                    JSONObject jobj = findISBNJSON.readJsonFromUrl(url+term1+"+"+term2);
                    
                    if(processJSON(jobj,names,title,tolerance))
                    {
                        return retObj;
                    }
                }
            }
        }
        System.out.println(url+term1);
        JSONObject jobj = findISBNJSON.readJsonFromUrl(url+term1);
        
        if(processJSON(jobj,names,title,tolerance))
        {
            return retObj;
        }
        return null;
    }
    //gets the query as search term
     private static String getTerm(String field,String value) 
    {
        String enc=null;
        try {
            enc = URLEncoder.encode(value.trim(),"UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EnhanceBibTex.class.getName()).log(Level.SEVERE, null, ex);
        }
        String request="";
        request+=field+":"+enc;
        return request;
    }
     private static boolean processJSON(JSONObject json, String names[],String title,double tolerance)
     {
         if((Integer)json.get("totalItems")>0)
         {
             
            JSONArray jarr=json.getJSONArray("items");            
            for(int i=0;i<jarr.length();i++)
            {
                JSONObject book = jarr.getJSONObject(i).getJSONObject("volumeInfo");
                
                
                if(similarity(book.get("title").toString().toLowerCase(),title.toLowerCase().trim()) > tolerance)
                {                    
                   // System.out.println("Probable Title Match:"+book.get("title").toString());
                    JSONArray auth = book.getJSONArray("authors");
                    int numAuth = names.length;
                    System.out.println(numAuth);
                    int matchAuth = 0;
                    for (int j = 0; j < auth.length(); j++) 
                    {
                        //System.out.println("MA:"+matchAuth);
                        String author = auth.getString(j);
                        boolean thisAuth = false;
                        for(String name: names)
                        {
                            String words[] = name.split("[\\s]+");
                            for(String word: words)
                            {
                                if(word.length()>1)
                                {
                                     if(author.toLowerCase().contains(word.toLowerCase()))
                                     {
                                         //System.out.println("Author match!!! +1 "+name);
                                         matchAuth++;
                                         thisAuth = true;
                                         break;
                                     }
                                }
                            }
                            if(thisAuth)
                                break;
                        }
                        if(matchAuth >= numAuth/2+1)
                        {
                            retObj = jarr.getJSONObject(i);
                            return true;
                        }
                    }
                }
            }
         }                
         return false;
     }
     //simple code to determine the similarity of two strings
     //Courtesy of acdcjunior@stackoverlow
    public static double similarity(String s1, String s2) 
    {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
          longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
        return (longerLength - StringUtils.getLevenshteinDistance(longer, shorter)) / (double) longerLength;
    }
    //DEBUG CODE
//     public static void main(String args[])
//     {
//         System.setProperty("https.proxyHost", "10.3.100.207");          //Set the IITKGP proxy
//         System.setProperty("https.proxyPort", "8080");  
//         BibTexData sample = new BibTexData();
//         sample.addPair("title","Core Java (TM) Volume 1: Fundamentals (For Anna University), 8/e");
//         sample.addPair("author","Horstmann, Cay S");
////         System.out.println(similarity("core java(tm) volume 1: fundamentals (for anna university), 8/e","Core Java (TM) Volume 1: Fundamentals (For Anna University), 8/e"));
//         try
//         {
//            fetchData(sample);
//         }
//         catch(IOException e)
//         {
//             e.printStackTrace();
//         }
//     }
}
