package metaExtract;

import java.io.*;
import javax.xml.parsers.*;
import static metaExtract.MainPage.*;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.*;

public class getReferences {

    public getReferences(String file) {
        System.out.println("in references");
        try {
            ProcessBuilder pbuilder = new ProcessBuilder("pdf-extract", "extract", "--references", file);

            File outXML = new File(FilenameUtils.removeExtension(filePath) + "references" + ".xml");
            pbuilder.redirectOutput(outXML);
            Process p = pbuilder.start();
            p.waitFor();

            BufferedReader br = new BufferedReader(new FileReader(FilenameUtils.removeExtension(filePath) + "references" + ".xml"));

            if (br.readLine() != null) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(outXML);

                try ( //Getting the writers ready
                        FileWriter fr = new FileWriter(FilenameUtils.removeExtension(filePath) + "-ref.txt", false)) {
                    PrintWriter pr = new PrintWriter(fr);
                    //Root element - outlines
                    Element root = doc.getDocumentElement();
                    NodeList nList = root.getElementsByTagName("reference");
                    //loop over each outline element
                    for (int i = 0; i < nList.getLength(); i++) {
                        String temp = nList.item(i).getTextContent();
                        //System.out.println((i + 1) + "---" + temp + "\n");
                        pr.println((i + 1) + "---" + temp + "\n");
                        //}
                    }
                    //closing resources
                    pr.close();
                }
                br.close();
            
            }

        } catch (Exception ex) {
            try {
                System.out.println("exception thrown in references");
      
                // Logger.getLogger(getReferences.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex1) {
                //Logger.getLogger(getReferences.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private static void RunShellCommandFromJava(String command) throws Exception {

        try {

            Process proc = Runtime.getRuntime().exec(command);

            proc.waitFor();
        } catch (IOException | InterruptedException ie) {
             System.out.println("exception thrown in references");
            throw ie;
        }
    }
}
